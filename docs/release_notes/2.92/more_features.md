# Blender 2.92: More Features

## Compositor

- Keying node in compositor ensures result is properly premuiltiplied
  (blender/blender@f68c3d557aa)
  In practice this means that there is no need in manually adding Alpha
  Convert node after the keying one. The existing files might need to be
  adjusted in this regard: the alpha convert node is to be removed if it
  present.

<!-- -->

- The Save as Render option was added to the File Output node. This
  feature allows to disable the use of the scene's color management
  options for each input socket individually.
  (blender/blender@27b78c9c94ba)

<!-- -->

- New compositor node to adjust the exposure of images.
  (blender/blender@6538f1e600ad)

<figure>
<video src="../../../videos/0001-1322.mp4" width="600" controls=""></video>
<figcaption>Demo by Carlo Bergonzini</figcaption>
</figure>

## Compatibility

- Pre-2.50.9 multires code has been removed
  (blender/blender@d11e357824d)
- Removed Simple multires subdivision type
  (blender/blender@17381c7b90e)
- "Camera View Lock" is now in the object relations panel instead of the
  preferences
  (blender/blender@00374fbde2bd)

## Motion tracking

- Reorganize menus to make them consistent with the rest of Blender and
  expose items previously only visible in panels
  (blender/blender@6c15b702796e)
- Simplified configuration of intrinsics to refine
  (blender/blender@0269f0c5745)
- Moved optical center to lens settings panel
  (blender/blender@24686351746)
- Solved random crashes tracking multiple tracks
  (blender/blender@29401d38d11)
- Huge speedup of tracking multiple tracks
  (blender/blender@5d130826221)

## Sequencer

- Media transform redesign
  (blender/blender@e1665c3d31)
- Background rectangle option for text strip
  (blender/blender@235c309e5f86)
- Add Overlay popover panels
  (blender/blender@fad80a95fd08)
- Paste strips after playhead by default
  (blender/blender@dd9d12bf45ed)
- Move remove gaps operator logic to module code
  (blender/blender@9e4a4c2e996c)
- Hide cache settings and adjust defaults
  (blender/blender@f448ff2afe7a)

## macOS

- Follow system preference for natural trackpad scroll direction
  automatically, remove manual preference
  (blender/blender@055ed33)
