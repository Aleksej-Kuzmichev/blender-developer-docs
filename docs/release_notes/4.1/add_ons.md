# Blender 4.1: Add-ons

## Rigify

- Nested bone collections are now supported by the Rig Layers UI: when a parent
  collection is hidden, buttons for its children are greyed out.
  (blender/blender-addons@69f9e45f7b439)
- Some custom properties now use recently added boolean (checkbox/toggle) and enum (dropdown) types. (blender/blender-addons@0585a98f16c039cd6, [Manual](https://docs.blender.org/manual/en/4.1/addons/rigging/rigify/rig_features.html#limbs))

## glTF 2.0

### Importer

#### New features & enhancements

- Implement KHR_materials_anisotropy import
  (blender/blender-addons@d6b9e57135bf4d392edfe1e2b93b91867e9c98de)
- Refactor material import
  (blender/blender-addons@f407b2075243ffbf98760ab88805fc5c716ee295,
  blender/blender-addons@c689c7b5c6ec3cb03bc8f28057099c1cbfd5dedb,
  blender/blender-addons@3b0b64b01e05d61222d9e1393119c462c36bf233,
  blender/blender-addons@5bf2cbd6b9d94f16a60f8b312760235d0ae2bc1d)
- Convert pbrSpecularGlossiness for Principled v2
  (blender/blender-addons@0d3045d8f6b2463af7db295fba24d1dac67401a2)
- Perf: Use NumPy for TRIANGLE STRIP and TRIANGLE FAN mode primitives
  (blender/blender-addons@8e33a044e7d4315ade1201c279834b3497d12588)
- Updates for Blender Python API changes from Blender 3.2 to 4.1
  (blender/blender-addons@ba5edfcb15ea5ca9a75570fbc7bfe1839fbdc0fd)

#### Fixes

- Use channel_packed at import
  (blender/blender-addons@74834adca02df21de459ab258e0287b7dd6c78b2)
- Avoid crash when custom property type is not managed
  (blender/blender-addons@0c775943251d78a20282b93c834bd3dd91bd50db)
- Change naming format of imported Color Attribute, to follow Blender naming
  (blender/blender-addons@650caaa5fb6e1b5e4e20a39198a3bc4b5aca6321)
- Fix importing certain textures with no source
  (blender/blender-addons@8b0c3b5bade05cd24b0af881177c2884072ee430)
- Change scaling factor of default custom bones
  (blender/blender-addons@c10a443bedf9cbf3319d13cbc9b692f78283419b)
- Remove no more needed check
  (blender/blender-addons@01e53a53c44efaa3f0d39618c274b452ad13d7df)

### Exporter

#### New features & enhancements

- Experimental GN instances export
  (blender/blender-addons@c24d76a44e5bf1f0a724becbf7a2e7a003a15901)
- Implement KHR_materials_anisotropy export
  (blender/blender-addons@d6b9e57135bf4d392edfe1e2b93b91867e9c98de)
- Add option to filter actions
  (blender/blender-addons@794446d1cae2da7b877a6e11d8fd59654f6986c8)
- Manage udim at export (conversion)
  (blender/blender-addons@666b21d1b0a0afc110df781f4508db1cb8a215b2)
- Add option to use shared accessors
  (blender/blender-addons@625590cb21cf646b9f1407e0ab7e9daef701bbf1)
- Option to export full collection hierarchy
  (blender/blender-addons@3f7c77cf187418290f65984bfd39ec818bd3b10f)
- Option to export unused images & textures
  (blender/blender-addons@991a42b30df1fc40a3b6d23135ac6a1c0e62c185,
  blender/blender-addons@a5d223459eb7f28de84552a15d6803d8b183c499)
- Add postprocessing using gltfpack
  (blender/blender-addons@97d4736cf060a3f5d0281df88d87c3d065f92c7b)
- Manage shader node groups traversal
  (blender/blender-addons@b80a0232de9ac2c34a0795fabba0af99041d5726)
- Remove armature object at export when possible
  (blender/blender-addons@1ea6ea87bd7b9efb9dc4dbd6bacf6f54ae7bcc94)
- Add option to flatten scene hierarchy
  (blender/blender-addons@a388a95ba710f4a669979e0487205480868d15b7)
- Manage texture direcly plugged when possible
  (blender/blender-addons@7f99be2c7df3f90cacf4c24d18ae115638a1ec16)
- Export VC only when needed
  (blender/blender-addons@90a3c155890e826b4ace6779c2d15a105e1dd878)
- Add a mesh hook 
  (blender/blender-addons@770600758d3631a5edc2aa89680951e683980471)

  
  
#### Fixes & maintenance
- Add glTF Embedded format option back - with option
  (blender/blender-addons@7a5c650a016bd1044bef2b7c49d9f9f32b515ba7)
- Update needed after autosmooth removal
  (blender/blender-addons@b1fbf73a08cfb68b14bb328590a9bb6602d96916)
- Fix crash when sk driver on non deformation bone when it's not exported
  (blender/blender-addons@607e95a087a8fd8dd21d0aa69de44cc807f5e134)
- Fix regression in image quality option
  (blender/blender-addons@75c8369bbd5587e72caf9e7859028e232bb2c5f2)
- Fix crash when NaN in SK
  (blender/blender-addons@08a87a6e993da4fd814766b631313444522fe121)
- Avoid crash when curve has armature modifier
  (blender/blender-addons@6ad4928ff31493b4edfa0bf7d35efe1ce5de3de3)
- Check parent type to determine if a node is really a child of a bone
  (blender/blender-addons@19768924dab3b134a2a77104f1e8a077125a5aa1)
- Fix exporter Vertex Color when no material
  (blender/blender-addons@2dfdcb0f7ee7675f259610413c5b21846499068e)
- Fix metallicRougness factors
  (blender/blender-addons@62b9a6c9dab2390571cf666258d194f3bed46e49)
- Fix exporting animation when gpu instancing is enabled
  (blender/blender-addons@e936f80134c14daa73bca1ac7331afbab03fd2c2)
- Take fps_base into account
  (blender/blender-addons@720c357b7f09175cff064f1f09c5016eb683981a)
- Check glTF node group with suffixes too
  (blender/blender-addons@433dfd5368d2608d433046911b9a52f350a55416)
- Add a check to avoid crash when there is a mesh without any data in it
  (blender/blender-addons@55716a27dd509d83c80499856e1ae605de5dbf86)
- Fix Checking material id when apply modifiers
  (blender/blender-addons@8e204c2e9f9e1eed8e02431cf992481fdf4e81e7)
- Check VC directly on Base Color socket
  (blender/blender-addons@3272f2702d2dec410cfee7e6e4e7b0f511b4c25b)
- 1 keyframe actions must not have CUBISPLINE interpolation
  (blender/blender-addons@7d13b7b06dbcd1386b72f9b89eb79d915d39c7c5)
- Fix UVMap as Custom Attribute FLOAT2 or FLOAT3
  (blender/blender-addons@47c669dcf84f21972582cd4d62b7b79933c50204)
- Perf: Find unique structured array elements more efficiently
  (blender/blender-addons@a8111fc2963ea07b68fe7094b88b8d425854e498)
- Updates for Blender Python API changes from Blender 3.2 to 4.1
  (blender/blender-addons@ba5edfcb15ea5ca9a75570fbc7bfe1839fbdc0fd)
  