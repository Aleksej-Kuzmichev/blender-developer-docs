# Blender 4.1: Sculpting

- Voxel remesh attribute preservation has been changed to propagate
  *all* attributes and to improve performance
  (blender/blender@ba1c8fe6a53a00c9324607db6a1836132cef5352).

- Add brush settings for view and normal automasking values
  (blender/blender@1e4f133950d48ce3c41e68ed8431b89176ea9433).

- Add brush setting for input samples 
  (blender/blender@a2b3fe5e013718d922d9cb3dd146e3d2e6af1419).

- Add scene setting for automasking propagation step value 
  (blender/blender@c61d1bcb54a6a610fb077eb4037ec93826f7c9fd).
