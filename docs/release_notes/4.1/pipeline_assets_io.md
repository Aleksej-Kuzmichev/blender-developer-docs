# Blender 4.1: Import & Export

## New File Handler API
File Handlers allow developers to extend traditional file "import" operators with drag-and-drop behavior throughout the Blender UI. (blender/blender@7c5fa8bf6cd, blender/blender@1254fee5898)

Initial use of file handlers for drag-and-drop support has been enabled for the following file formats within the 3D Viewport and Outliner areas: Alembic, Collada, Grease Pencil SVG, OBJ, OpenUSD, PLY, and STL (blender/blender@d4bc3bd08d6)

Support for additional formats, notably those that are implemented as Python addons, will be added in the future.

Python API examples are available in the [documentation](https://docs.blender.org/api/master/bpy.types.FileHandler.html).

## USD

- New capabilities
  - Export armatures and shape keys (blender/blender@c6d61e8586).
  - Scene graph instancing import support (blender/blender@ea89e11e01).
  - Point instancing import support (blender/blender@fcd10ee33a).
  - Optionally author subdivision schema on export (blender/blender@5edda6cbcc).
  - Import can now be extended with Python scripts through hooks. See the USDHook API [documentation](https://docs.blender.org/api/master/bpy.types.USDHook.html) for details and example code (blender/blender@ecbf3385c56).

- Notable improvements
  - Export to a single root prim by default (blender/blender@b262655d39).
  - Author the extents property for lights on export (blender/blender@e6ff1348cb).
  - Add scale and bias processing for UsdUVTexture (blender/blender@f355b202621).
  - Add channel processing for UsdUVTexture (blender/blender@228ee8f1c319).

## Alembic

- Velocities on Points are now read (blender/blender@472cf44166).
- Render resolution is now written to cameras (blender/blender@5412bd48a9).

## STL

- New STL (.stl) exporter
  (blender/blender@17c793e43c66ba).
  The new exporter is written in C++ and is 3x-10x faster than the
  Python exporter.
- Python STL exporter/importer add-on has been marked as legacy. Users of
`bpy.ops.export_mesh.stl` / `bpy.ops.import_mesh.stl` are advised to switch to
new STL exporter/importer functions `bpy.ops.wm.stl_export` /
`bpy.ops.wm.stl_import`.

## FBX

- Normals are now exported as vertex normals when the
`Mesh.normals_domain` property is `'POINT'`.
  (blender/blender-addons@3d823efc57)
- Normals are now exported using the `IndexToDirect` FBX reference
  mode.
  (blender/blender-addons@791b042c3a)
  - Older versions of the FBX I/O add-on do not support importing vertex
    normals with this reference mode, but will still import FBX I/O
    exported meshes with the correct normals because the vertex normals
    that cannot be imported will match the normals of the imported mesh.
    Custom normals won't be set in this case.

## Stanford PLY

- PLY importer and exporter now supports custom vertex attributes
  (blender/blender@0eb6aef3b00).

## OBJ

- Exporting completly flat or smooth shaded meshes can be up 20-40% faster (blender/blender@94ca3aea83).

## 3DS

- Export auto smooth angle from ´Smooth by Angle ´modifier.
  (blender/blender-addons@9a9142f160)
  
## glTF 2.0

Find the changelog in the
[Add-ons](add_ons.md) section.
