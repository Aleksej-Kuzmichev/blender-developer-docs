# Blender 4.1: Animation & Rigging

## Bone Collections

**Bone Collections are now hierarchical** (blender/blender@129fb2eab8). The
collections are shown in a tree instead of a flat list, where they can be
rearranged and nested via drag-and-drop. The outliner also shows the bone
collection hierarchy (blender/blender@2e2b5dcd52).

![Screenshot of the Bone Collections in the Outliner in Blender 4.1](bone-collections-outliner.png){style="width:300px;"}

Visibility is determined by the bone collection itself, and its ancestors: a
bone collection is only visible when its parent, grandparent, etc. are visible
(blender/blender@6cfbf9ef2f). In other words: hiding a bone collection will also
hide its children.

![Screenshot of the Bone Collections in the Armature Properties panel in Blender 4.1](bone-collections-armature-properties.png){style="width:300px;"}

**Bone Collections can be 'solo'ed** with the ☆ icon. When *any* bone collection
is marked as 'solo', shown as&nbsp;★ in the interface, the regular visibility
options (the 👁&nbsp;icon) are overridden. Only the bone collections with
a&nbsp;★ are then shown. This makes it possible to, for example, quickly show
certain bone collections without altering the regular visibility setup.

An **Un-Solo All** operator is available to disable all 'solo'ed bone
collections, and return to the regular visibility rules.

![Screenshot of the Bone Collections in the Bone Properties panel in Blender 4.1](bone-collections-bone-properties.png){style="width:300px;"}

The Bone properties panel also shows which Bone Collections the Bone is assigned
to, including visibility & solo toggles. The bone can also be un-assigned from
its collections here.


### Python API

```py
import bpy

# Create bone collections
armature = bpy.context.object.data
bcoll_root = armature.collections.new("A Root Collection")
bcoll_child = armature.collections.new("Child Collection", parent=bcoll_root)

# Moving the bone collection after it has been created.
bcoll = armature.collections.new("collection")
bcoll.parent = bcoll_root  # Assign a new parent collection.
bcoll.child_number = 0     # Move to be the first of its siblings.

# Access to the top level (aka 'root') collections:
for bcoll in armature.collections:
    print(f'Root collection: {bcoll.name}')

# Access to all collections:
for bcoll in armature.collections_all:
    print(f'Collection: {bcoll.name}')

# Assigned bones can be retrieved hierarchically:
bcoll_child.assign(armature.bones['thigh.L'])
for bone in bcoll_root.bones_recursive:
    print(bone.name)
```

## NLA

- Add new channel options to Action bake
  (blender/blender@dd5b870d15287e8322c799edef9823e3c9a8bda2).
- Add bake custom properties to Action bake (blender/blender@4ddb52a775950fe14733f675a4804883a2e4dce3).
- Rename "NLA Channels" to "NLA Tracks"
  (blender/blender@661e7e451a0eadd7955a6074a79d570ac4ce2927).

| New Bake Options                                        | Bake Custom Properties                                            |
|---------------------------------------------------------|-------------------------------------------------------------------|
| ![](../../../images/Action_bake_options.png){width=300} | ![](../../../images/Action_bake_custom_properties.png){width=300} |

## Weight Paint

### Bone Selection

The bone selection mode is made explicit when you enter weight paint
mode with an armature. It now has an icon and can be accessed with the
hotkey `3`. The following selection tools have been added to make bone
selection easier:

- Tweak

Commits:
blender/blender@edcac1f48b92aee693f01a9bb4d23c03870a8f29

## Keying

- Pressing `I` in the viewport will no longer pop up a menu of keying
  sets. Instead it will read the User Preferences which has a new
  setting to define which channels get keyed.
  (blender/blender@a99e419b6e841d673a3542c372d5faec82c0d138)
- There is a new hotkey `K` in Object and Pose Mode which pops up the Keying Set menu. This shows the menu even if a keying set is active in the scene. (blender/blender@87fc8e8dddf6481030a956aaa77c2cde9c72d03f)
- The hotkey to change the active Keying Set has been changed from `Ctrl+Shift+Alt+I` to `Shift+K`. (blender/blender@87fc8e8dddf6481030a956aaa77c2cde9c72d03f)
- When the User Preference option "Pie Menu on Drag" is enabled, holding `I` and moving the cursor will show a pie menu to insert one of Location, Rotation, Scale and Available. (blender/blender@87fc8e8dddf6481030a956aaa77c2cde9c72d03f)
- The User Preference option "Only Insert Needed" is now split between manual keying and auto-keying.
  (blender/blender@5e28601d693938525c3a23dba81f9307ab6302cd)

## Drivers
- Single Property and Context Property driver variables now support a fallback value to use if the RNA path lookup fails. (blender/blender@d0ef66ddff58065dbda08d32c06ef3157ff6487d, [Manual](https://docs.blender.org/manual/en/4.1/animation/drivers/drivers_panel.html#bpy-types-drivervariable-type-context-prop))
- Drivers that failed to evaluate are now underlined in red in the channel list of the Driver Editor. Previously that only happened for drivers attached to non-existing properties. (blender/blender@b9074381e5d1947cfd5aeebbd48e0cef9d6163f8)

## Graph Editor
- Scale from Neighbor operator (blender/blender@b27718a9e73a8e5ae29fb68a439ec76fbb5d8080, [Manual](https://docs.blender.org/manual/en/4.1/editors/graph_editor/fcurves/editing.html#scale-from-neighbor))
- Add an option to automatically lock key movement to either the X or Y axis. This can be found under <span class="literal">View</span> » <span class="literal">Auto-Lock Axis</span>. (blender/blender@446b92d2cecab1caa6c7aa1b63ae822461b9144b)
- Add option to right click menu on animated properties to view the
  FCurve that animates it. For this to work the object/node has to be selected.
  (blender/blender@a91a8f3fed8c54574b73b9187f004cf0632691da, [Manual](https://docs.blender.org/manual/en/4.1/editors/graph_editor/introduction.html#navigation))

<figure>
<video src="../../../videos/View_fcurve.mp4" title="Demo of viewing the F-Curve of the animated property" width="720" controls=""></video>
<figcaption>Demo of viewing the F-Curve of the animated property</figcaption>
</figure>

### F-Curve Baking

A new operator "Bake Channels" has been added to the Graph Editor. (blender/blender@1e931f5bd7e5177071efec26463a29034d955616, [Manual](https://docs.blender.org/manual/en/4.1/editors/graph_editor/channels/editing.html#bake-channels))

It allows you to:

- Specify a range to bake.
- Define the distance between baked keys, e.g. 2s or 3s.
- Remove keys outside the baked range.
- Define a interpolation type for new keyframes.
- Bake modifiers to keyframes.

It is found in the Graph Editor under
<span class="literal">Channel</span> » <span class="literal">Bake Channels</span>.

## Dope Sheet

- Speed up Dope Sheet by only calculating keyframes that are visible in
  the current view.
  (blender/blender@f06fd85d970f5b37ceb50a2eca6dc766a4b9de8e)

## Motion Paths
- Add an option to create motion paths relative to the active camera. This means the motion paths will appear in screen space when looking through that camera. (blender/blender@79f84775f2655db22642f3699f53d3fae8097831)
<figure>
<video src="../../../videos/release_notes_4_1_camera_space_motion_paths.mp4" title="Demo of motion paths in camera space" width="720" controls=""></video>
<figcaption>Demo of motion paths in camera space</figcaption>
</figure>
