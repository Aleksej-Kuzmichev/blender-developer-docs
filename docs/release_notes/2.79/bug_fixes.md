# Blender 2.79: Bug Fixes

Changes from revision
blender/blender@27c64e3f46
to
blender/blender@f934f54b84,
inclusive (*master* branch).

\[RC2\] Changes from revision
blender/blender@5e9132b3b7
to
blender/blender@7b397cdfc8,
inclusive (*blender-v2.79-release* branch).

\[Release\] Changes from revision
blender/blender@beea9421bd
to
blender/blender@8ef39d5c88,
inclusive (*blender-v2.79-release* branch).

Total fixed bugs: 789 (416 from tracker, 373 reported/found by other
ways).

**Note:**Before RC1 (i.e. during regular development of next version in
master branch), only fixes of issues which already existed in previous
official releases are listed here. Fixes for regressions introduced
since last release, or for new features, are **not** listed here.  
For following RCs and final release, **all** backported fixes are
listed.

## Objects / Animation / GP

### Animation

- Fix [\#50026](http://developer.blender.org/T50026): "Only Insert
  Needed" doesn't work when using Trackball rotation
  (blender/blender@8b2905952e).
- Fix [\#49816](http://developer.blender.org/T49816): Keyframing NLA
  Strip influence from Python set keyframes in the wrong place
  (blender/blender@3cbe95f683).
- Fix [\#50842](http://developer.blender.org/T50842): NLA Influence
  Curve draws out of bounds when it exceeds the 0-1 range
  (blender/blender@91ce13e90d).
- Fix [\#50904](http://developer.blender.org/T50904): Imprecise timeline
  frame selection using mouse
  (blender/blender@17689f8bb6).
- Fix [\#51867](http://developer.blender.org/T51867): Insert Keyframe
  I - L / I - R / I - S key combos are broken
  (blender/blender@7abed4e433).
- Fix [\#51792](http://developer.blender.org/T51792): crash calling
  bpy.ops.screen.animation_cancel while scrubbing
  (blender/blender@f51438aea0).
- Fix [\#50973](http://developer.blender.org/T50973): Directional blur
  node doesn't clamp value if using driver
  (blender/blender@00d476976e).
- Fix [\#51879](http://developer.blender.org/T51879): NLA Influence can
  not be autokeyed
  (blender/blender@b452003015).
- Fix [\#52009](http://developer.blender.org/T52009): F-Curve "Stepped
  interpolation" modifier "restrict frame-range" IN and OUT parameters
  cannot be edited
  (blender/blender@fdfcbfd040).
- Fix [\#52058](http://developer.blender.org/T52058): Jumping to
  keyframe causes Blender to freeze indefinitely
  (blender/blender@7021aa245d).
- Fix [\#52213](http://developer.blender.org/T52213): Enum drivers no
  longer work
  (blender/blender@87d5e34453).
- \[RC2\] Fix [\#52327](http://developer.blender.org/T52327):
  Entering/Exiting NLA Tweakmode disables Scene -\> Only Keyframes from
  Selected Channels
  (blender/blender@3689be736b).
- \[RC2\] Fix [\#52401](http://developer.blender.org/T52401): "Export
  Keying Set" operator generated incorrect ID's for shapekeys
  (blender/blender@7b397cdfc8).
- \[Release\] Fix [\#52227](http://developer.blender.org/T52227): Time
  Slide tool doesn't take NLA mapping into account
  (blender/blender@73cdf00ea8).

<!-- -->

- Fix unreported: Fixes for pose library change 601ce6a89c4
  (blender/blender@305fec8358).
- Fix unreported: Depsgraph: Fix some errors printed to the console
  (blender/blender@630c0559f9).
- Fix unreported: Also apply similar fixes to .keyframe_delete()
  (blender/blender@b1c6ddb107).
- Fix unreported: Fix: NLA "Strip Time" setting cannot be edited
  (blender/blender@65582e75e3).
- Fix unreported: fix: redraw dope sheet / action editor when pose bone
  selection changes
  (blender/blender@502c4be56e).
- Fix unreported: A bunch of fixes for Pose Library while checking on
  [\#51607](http://developer.blender.org/T51607)
  (blender/blender@085ed00e42).
- Fix unreported: Fix fcurve color assignment
  (blender/blender@2b7edb77c9).
- \[Release\] Fix unreported: Fix: GPencil Sequence Interpolation for
  thickness/strength was inverted
  (blender/blender@ed0429e7e6).
- \[Release\] Fix unreported: Fix: Border select for GPencil keyframes
  was including those in the "datablock" channels even though those
  weren't visible
  (blender/blender@f5d02f055f).

### Constraints

- Fix [\#49981](http://developer.blender.org/T49981): When camera is on
  inactive layer, it does not evaluate constraints
  (blender/blender@24676b535a).

<!-- -->

- Fix unreported: Fix (unreported) missing update when adding constraint
  from RNA
  (blender/blender@d66d5790e9).
- Fix unreported: Fix/workaround
  [\#50677](http://developer.blender.org/T50677): Shrinkwrap constraint
  don't get updated when target mesh gets modified
  (blender/blender@2342cd0a0f).
- Fix unreported: Fix another part of
  [\#50565](http://developer.blender.org/T50565): Planar constraints
  were always initialized to accurate transform
  (blender/blender@87f8bb8d1d).

### Grease Pencil

- Fix [\#50051](http://developer.blender.org/T50051): Avoid crash when
  render grease pencil from VSE
  (blender/blender@e400f4a53e).
- Fix [\#50081](http://developer.blender.org/T50081): Grease pencil
  parented rotation problem
  (blender/blender@dd82d70bc5).
- Fix [\#50123](http://developer.blender.org/T50123): GreasePencil:
  Modifying name of new color in new palette via bpy segfaults
  (blender/blender@e2d223461e).
- Fix [\#50264](http://developer.blender.org/T50264): Stroke is not
  painted when append Grease Pencil block
  (blender/blender@535298eea5).
- Fix [\#50184](http://developer.blender.org/T50184): Grease Pencil
  Layer synchronization between Dope Sheet and Properties panel
  (blender/blender@c5338fd162).
- Fix [\#49617](http://developer.blender.org/T49617): Grease Pencil
  Stroke Placement regression
  (blender/blender@15215652e1).
- \[Release\] Fix [\#52483](http://developer.blender.org/T52483): Fill
  is incorrect for interpolated strokes
  (blender/blender@27c42e05c4).
- \[Release\] Fix [\#52678](http://developer.blender.org/T52678): Crash
  editing gpencil w/ frame-lock
  (blender/blender@3aaf908719).
- \[Release\] Fix [\#52650](http://developer.blender.org/T52650): Grease
  pencil selection its not automatically updating in Clip Editor
  (blender/blender@87cc8550e2).

<!-- -->

- Fix unreported: Fix Grease Pencil render in VSE crashes when no strips
  (#[\#49975](http://developer.blender.org/T49975))
  (blender/blender@0b9b8ab2dd).
- Fix unreported: Grease Pencil: Fix python errors opening N panel -\>
  GP with empty VSE
  (blender/blender@89c1f9db37).
- Fix unreported: GPencil: Fix unreported error in animation after
  rename items
  (blender/blender@196520fe7d).
- Fix unreported: Fix: Avoid creating redundant frames when erasing
  (blender/blender@7452af0f86).
- Fix unreported: Fix: Make it possible to erase strokes (on other
  layers) even if the active layer doesn't have any frames
  (blender/blender@00edc600b0).
- Fix unreported: Fix minor glitches in GP code
  (blender/blender@475d536f72).
- Fix unreported: GPencil: Fix unreported animation data missing when
  change palette name
  (blender/blender@520afa2962).
- Fix unreported: Fix: GPencil delete operators did not respect color
  locking
  (blender/blender@117d90b3da).
- Fix unreported: Fix missing NULL check in gpencil poll
  (blender/blender@cb6ec44fc7).
- Fix unreported: GPencil Copy/Paste Fix: Copying/Pasting strokes
  between datablocks would crash
  (blender/blender@89d4164f54).
- Fix unreported: GP Copy/Paste Fix: Paste button doesn't update after
  copying strokes using Ctrl-C
  (blender/blender@7667040dd0).
- Fix unreported: Fix: Pasting GP strokes files between files (or when
  the original colors were deleted) would crash
  (blender/blender@2bd49785f9).
- Fix unreported: Fix: GP Clone brush was not correcting color
  references for pasted strokes either
  (blender/blender@d25ab3fcc4).
- Fix unreported: Grease Pencil: Fix hardcoded DKEY for continous
  drawing
  (blender/blender@c2d285f0ff).
- \[Release\] Fix unreported: Fix: Deleting GPencil keyframes in
  DopeSheet didn't redraw the view
  (blender/blender@a1e8ef264f).

### Objects

- Fix [\#49743](http://developer.blender.org/T49743): Adding torus in
  edit mode local mode shows error
  (blender/blender@216a3a3826).
- Fix [\#50008](http://developer.blender.org/T50008): camera DOF
  Distance picking from W key menu not working
  (blender/blender@43703fa4bf).
- Fix [\#50078](http://developer.blender.org/T50078): Vertex Groups not
  copied over when making proxy
  (blender/blender@8c93178c96).
- Fix [\#49718](http://developer.blender.org/T49718): Wrong "Make
  Duplicates Real" behavior with "Keep Hierarchy"
  (blender/blender@99c5c8befc).
- Fix [\#50369](http://developer.blender.org/T50369): Objects can't be
  deleted from scene when using "link group objects to scene"
  (blender/blender@351a9d084f).
- Fix [\#50084](http://developer.blender.org/T50084): Adding torus
  re-orders UV layers
  (blender/blender@9f67367f0a).
- Fix [\#49860](http://developer.blender.org/T49860): Copying vgroups
  between objects sharing the same obdata was not possible
  (blender/blender@f3a7104adb).
- Fix [\#51625](http://developer.blender.org/T51625): fix impossibility
  to delete uninstantiated objects from Outliner
  (blender/blender@08b7955415).
- Fix [\#51657](http://developer.blender.org/T51657): ID user count
  error when deleting a newly created object with an assigned
  dupli_group
  (blender/blender@f212bfa3cd).
- Fix [\#51883](http://developer.blender.org/T51883): Wrong matrix
  computation in "Make Duplicates Real"
  (blender/blender@44397a7a0a).

<!-- -->

- Fix unreported: Fix (unreported) asserts in
  `make_object_duplilist_real()`
  (blender/blender@17fb504bcf).
- Fix unreported: Fix UV layer bug in object_utils.object_data_add()
  (blender/blender@b8710e1468).
- Fix unreported: Fix Torus default UV's offset outside 0-1 bounds
  (blender/blender@d30a0239a2).
- Fix unreported: Fix memory leak when making duplicates real and parent
  had constraints
  (blender/blender@cd5c853307).
- Fix unreported: Fix race condition invalidating object data's bounding
  box
  (blender/blender@e6954a5a7c).
- Fix unreported: MakeLocal: fix bad (missing) handling of proxy_from
  uglyness
  (blender/blender@622ce5672a).

### Dependency Graph

- Fix [\#49836](http://developer.blender.org/T49836): Partial Camera DOF
  properties not updating via graph editor
  (blender/blender@42a91f7ad8).
- Fix [\#49826](http://developer.blender.org/T49826): NEW-DEPSGRAPH -
  Texture is not updated after changing its space color
  (blender/blender@647255db93).
- Fix [\#49993](http://developer.blender.org/T49993): Indirectly used
  taper/bevel crashes new dependency graph
  (blender/blender@7dda3cf830).
- Fix [\#49994](http://developer.blender.org/T49994): Setting dupligroup
  which uses indirect relation will crash
  (blender/blender@7d33d4439f).
- Fix [\#50060](http://developer.blender.org/T50060): New depsgraph does
  not update mask animation
  (blender/blender@1f8762bb8e).
- Fix [\#49981](http://developer.blender.org/T49981): New Depsgraph -
  When camera is on inactive layer, it does not evaluate constraints
  (blender/blender@25c534f20a).
- Fix [\#50512](http://developer.blender.org/T50512): Linked Backround
  scene with animation not updating with new depsgraph
  (blender/blender@e29a6f739d).
- Fix [\#50331](http://developer.blender.org/T50331): New Dependency
  Graph - "frame" python driver expression not working
  (blender/blender@a90622ce93).
- Fix [\#50938](http://developer.blender.org/T50938): Cache not being
  reset when changing simulation settings with new depsgraph
  (blender/blender@9ad252d157).
- Fix [\#51318](http://developer.blender.org/T51318): Non-update of
  preview when switching from world to lamp panel
  (blender/blender@849e77b1f9).
- Fix [\#52134](http://developer.blender.org/T52134): New depsgraph
  crashes when evaluating several psys on from object
  (blender/blender@4d67034076).
- \[RC2\] Fix [\#52255](http://developer.blender.org/T52255): New
  Depsgraph - Constraint and Drivers not working together when the
  driver references itself
  (blender/blender@b1d998ec5d).
- \[Release\] Fix [\#52454](http://developer.blender.org/T52454): Crash
  in DEG_graph_on_visible_update when activating scene layer
  (blender/blender@63e21e7218).
- \[Release\] Fix [\#51907](http://developer.blender.org/T51907): New
  Depsgraph - Camera constraint is not evaluated properly
  (blender/blender@edded659c6).
- \[Release\] Fix [\#52209](http://developer.blender.org/T52209): New
  Depsgraph - animated follow curve constraint sometimes freaks out when
  the curve has a parent
  (blender/blender@0bee126977).

<!-- -->

- Fix unreported: Depsgraph: Fix race condition writing drivers to array
  property
  (blender/blender@643c5a24d5).
- Fix unreported: Depsgraph: Fix wrong comparison of ID type vs. node
  type
  (blender/blender@fc1b35e44c).
- Fix unreported: Depsgraph: Fix wrong relation from IK solver to pole
  target
  (blender/blender@aef66a6be0).
- Fix unreported: Depsgraph: Fix another issue which seems to be a bug
  (blender/blender@f0d53ac109).
- Fix unreported: Depsgraph: Fix wrong relations in array modifier
  (blender/blender@4710fa700c).
- Fix unreported: Depsgraph: Fix missing ID node tag
  (blender/blender@653541ea78).
- Fix unreported: Depsgraph: Fix typo in text on curve relation builder
  (blender/blender@b1743cda5a).
- Fix unreported: Depsgraph: Fix missing DONE flag in relations builder
  (blender/blender@24d89a1f77).
- Fix unreported: Depsgraph: Fix frash with iTaSC solver
  (blender/blender@af0e6b31a5).
- Fix unreported: Depsgrpah: Fix missing animation update in movie clips
  (blender/blender@d294509dd8).
- Fix unreported: Fix copy/paste typo in new depsgraph object geometry
  builder (found by coverity)
  (blender/blender@b859fef670).
- Fix unreported: Depsgraph: Fix infinite viewport object update in
  CYcles render mode
  (blender/blender@5eab3b079f).
- Fix unreported: Depsgraph: Fix matrix_world driver source
  (blender/blender@f2b57c3532).
- Fix unreported: Fix depsgraph: hair collision is actually enabled, so
  add the relations
  (blender/blender@60dae91db8).
- Fix unreported: Depsgraph: Fix duplicated operation node when two
  objects are sharing same armature
  (blender/blender@605695de61).
- Fix unreported: Depsgraph: Fixed crash with curve bevel indirect
  dupligroups
  (blender/blender@0434053f13).
- Fix unreported: Depsgraph: Fix missing relations update tag when
  typing \#frame
  (blender/blender@dc5007648c).
- Fix unreported: Depsgraph: Fix missing updates when in local view
  (blender/blender@843be91002).
- Fix unreported: Fix missing relation in new DEG between World and its
  NodeTree
  (blender/blender@42c346028f).
- Fix unreported: Depsgraph: Fix/workaround crahs when fcu-\>rna_path is
  NULL
  (blender/blender@8eeb610832).
- Fix unreported: Depsgraph: Fix object being tagged for data update
  when it shouldn't
  (blender/blender@34cb934343).
- Fix unreported: Depsgraph: Fix missing relations for objects which are
  indirectly linked
  (blender/blender@d1d359b792).

## Data / Geometry

### Armatures

- Fix [\#50023](http://developer.blender.org/T50023): Inverse Kinematics
  angle limits defaulting to 10313.2403124°
  (blender/blender@6397319659).
- Fix [\#50393](http://developer.blender.org/T50393): Flip names working
  just on one side when both are selected
  (blender/blender@702bc5ba26).
- Fix [\#49527](http://developer.blender.org/T49527): Blender stalls
  when changing armature ghosting range with stepsize = 0
  (blender/blender@d2382f782e).
- Fix [\#50932](http://developer.blender.org/T50932): depth picking w/
  pose-bone constraints
  (blender/blender@810982a95c).
- Fix [\#51965](http://developer.blender.org/T51965): Custom Bone Shape
  Wireframe setting affects wireframe thickness
  (blender/blender@f86b43e130).
- Fix [\#51955](http://developer.blender.org/T51955): Changing Auto-IK
  length crashes Blender (with new-depsgraph)
  (blender/blender@a7a5c20fbc).
- Fix [\#51971](http://developer.blender.org/T51971): IK non-uniform
  scale regression
  (blender/blender@f05f2f0336).
- Fix [\#52224](http://developer.blender.org/T52224): auto IK not
  respecting length, after recent bugfix
  (blender/blender@becb413f29).

<!-- -->

- Fix unreported: Depsgraph: Fix crash deleting bones in armature edit
  mode
  (blender/blender@4d9562a3ae).
- Fix unreported: Fix crash happening in some cases with MakeLocal
  operator
  (blender/blender@ed957768a0).
- Fix unreported: Fix (unreported) crash when drawing armatures' poses
  in some cases
  (blender/blender@1be717d007).

### Curve/Text Editing

- Fix [\#50614](http://developer.blender.org/T50614): Curve doesn't
  restore initial form after deleting all its shapekeys
  (blender/blender@6f1493f68f).
- Fix [\#50745](http://developer.blender.org/T50745): Shape key editing
  on bezier objects broken with Rendered Viewport Shading
  (blender/blender@5e1d4714fe).
- Fix [\#51149](http://developer.blender.org/T51149): Joining curves
  allows 2D curves in 3D
  (blender/blender@97c9c6a3f3).
- Fix [\#51350](http://developer.blender.org/T51350): 2D curve normals
  flip when deformed
  (blender/blender@f78ba0df02).
- Fix [\#52007](http://developer.blender.org/T52007): Cancel bezier
  curve transform fails
  (blender/blender@2acd05b24c).
- Fix [\#51665](http://developer.blender.org/T51665): No orientation for
  nurbs, polygons
  (blender/blender@004a143760).
- Fix [\#39925](http://developer.blender.org/T39925): Set Origin to
  Geometry on a new text object moves the origin away from the object
  (blender/blender@06505c5264).

<!-- -->

- Fix unreported: Fix missing break setting curve auto-handles
  (blender/blender@406398213c).
- Fix unreported: Fix serious bug in 'curve-to-mesh' conversion code
  (blender/blender@7a80c34f52).

### Mesh Editing

- Fix [\#50046](http://developer.blender.org/T50046): Segmentation fault
  due to out-of-range VertexGroup.weight() call
  (blender/blender@209bc9977c).
- Fix [\#50003](http://developer.blender.org/T50003): Bevel makes
  non-manifold mesh
  (blender/blender@1de79c8960).
- Fix [\#49848](http://developer.blender.org/T49848): bevel of spiral
  gets bigger and bigger widths
  (blender/blender@3d243eb710).
- Fix [\#49807](http://developer.blender.org/T49807): Inset faces edge
  rail bug
  (blender/blender@1455023e64).
- Fix [\#50534](http://developer.blender.org/T50534): Part I, cleanup
  loop normals generated during modifier stack evaluation
  (blender/blender@fb2f95c91a).
- Fix [\#50524](http://developer.blender.org/T50524): Basis shapekey
  editing while rendering bug
  (blender/blender@9e2dca933e).
- Fix [\#50662](http://developer.blender.org/T50662): Auto-split affects
  on smooth mesh when it sohuldn't
  (blender/blender@efbe47f9cd).
- Fix [\#50855](http://developer.blender.org/T50855): Intersect (knife)
  w/o separate doesn't select
  (blender/blender@3caeb51d7f).
- Fix [\#50920](http://developer.blender.org/T50920): Adds missing edges
  on return of bisect operator
  (blender/blender@2b3cc24388).
- Fix [\#50950](http://developer.blender.org/T50950): Converting meshes
  fails w/ boolean
  (blender/blender@750c0dd4de).
- Fix [\#51038](http://developer.blender.org/T51038):
  `layerInterp_mloopcol` was casting instead of rounding the
  interpolated RGBA channels
  (blender/blender@d23459f516).
- Fix [\#51072](http://developer.blender.org/T51072): The reference of a
  pyobject may be being overwritten in `bm_mesh_remap_cd_update`
  (blender/blender@d097c2a1b3).
- Fix [\#51135](http://developer.blender.org/T51135): Cylinder primitive
  generated bad UVs
  (blender/blender@d426c335c5).
- Fix [\#51137](http://developer.blender.org/T51137): Edge Rip Hangs
  (blender/blender@a0799ce336).
- Fix [\#51184](http://developer.blender.org/T51184): Crash of Blender
  when I try to join an object with one that has booleans modifiers
  (blender/blender@6b815ae55d).
- Fix [\#51284](http://developer.blender.org/T51284): Mesh not skinned
  (blender/blender@573f6d1ec4).
- Fix [\#51180](http://developer.blender.org/T51180): BMesh crash using
  calc_uvs=True
  (blender/blender@df94f2f399).
- Fix [\#49467](http://developer.blender.org/T49467): Crash due to
  assert failure in bevel
  (blender/blender@8be9d68dd4).
- Fix [\#51539](http://developer.blender.org/T51539): BMesh boolean
  crash
  (blender/blender@208462e424).
- Fix [\#51520](http://developer.blender.org/T51520): Broken vertex
  weights after two mesh joining
  (blender/blender@e3d6321530).
- Fix [\#48668](http://developer.blender.org/T48668): bevel mistake on
  presumed reflex angle
  (blender/blender@49cc78ef18).
- Fix [\#51637](http://developer.blender.org/T51637): Mesh Tools - Noise
  Button Crashes
  (blender/blender@275e2fb0ff).
- Fix [\#48996](http://developer.blender.org/T48996): bevel bad for
  certain in-plane edges
  (blender/blender@9ba7805a3b).
- Fix [\#51520](http://developer.blender.org/T51520): Broken vertex
  weights after two mesh joining
  (blender/blender@2fb56e7157).
- Fix [\#50906](http://developer.blender.org/T50906): and
  [\#49361](http://developer.blender.org/T49361), bevel didn't curve in
  plane sometimes
  (blender/blender@538fe7d48e).
- Fix [\#51733](http://developer.blender.org/T51733): 3d print toolbox
  checks report false positives
  (blender/blender@c71e160c02).
- Fix [\#51648](http://developer.blender.org/T51648): Inconsistent edge
  collapse point depending on orientation
  (blender/blender@ab4b7b5368).
- Fix [\#51856](http://developer.blender.org/T51856):
  `BKE_mesh_new_from_object()` would often generate default 'Mesh'
  named datablock
  (blender/blender@af35455912).
- Fix [\#51900](http://developer.blender.org/T51900): Crash after
  pressing "F" multiple times
  (blender/blender@2c0ee61f6d).
- Fix [\#51100](http://developer.blender.org/T51100): Vertex pick fails
  after extrude
  (blender/blender@d3f7b04f04).
- Fix [\#52066](http://developer.blender.org/T52066): Grid Mesh UV's
  aren't correct
  (blender/blender@49c29dc82f).
- Fix [\#52176](http://developer.blender.org/T52176): Bevel doesn't
  correctly work with default empty Vgroup
  (blender/blender@66e28a2827).
- \[RC2\] Fix [\#52329](http://developer.blender.org/T52329): Boolean
  with aligned shapes failed
  (blender/blender@0cb38b2875).
- \[Release\] Fix [\#52434](http://developer.blender.org/T52434):
  Restore mesh center of mass calculation
  (blender/blender@ba600ff7fa).
- \[Release\] Fix [\#52588](http://developer.blender.org/T52588): Shape
  key value driver variables of duplicated object sets refer to old
  objects
  (blender/blender@e9ca9dd5d7).
- \[Release\] Fix [\#52251](http://developer.blender.org/T52251): Knife
  cur displaces surface
  (blender/blender@a8c7f1329b).
- \[Release\] Fix [\#52701](http://developer.blender.org/T52701): Mesh
  shortest path fails at boundaries
  (blender/blender@904831e62e).

<!-- -->

- Fix unreported: BMesh: fix edge-rotation selection state bug
  (blender/blender@d3919c22b0).
- Fix unreported: BMesh: fix edge-rotate with w/ flipped faces
  (blender/blender@77ba1ed5db).
- Fix unreported: Fix (unreported) looptri array not being recalculated
  in ccgDM and emDM
  (blender/blender@7fe7835d13).
- Fix unreported: Fix face-creation with existing hidden geometry
  (blender/blender@11187e8628).
- Fix unreported: Fix wrong loop normals left after face splitting
  (blender/blender@20283bfa0b).
- Fix unreported: Fix wrong edges created by split faces
  (blender/blender@40e5bc15e9).
- Fix unreported: Fix more corner cases failing in mesh faces split
  (blender/blender@9b3d415f6a).
- Fix unreported: Fix useless allocation of edge_vectors in threaded
  case of loop split generation
  (blender/blender@9d6acc34a1).
- Fix unreported: BMesh: Fix BM_face_loop_separate_multi
  (blender/blender@304315d181).
- Fix unreported: Fix unreported: inaccuracy of interpolation of custom
  color layers due to float truncation
  (blender/blender@a6f74453b6).
- Fix unreported: Fix skin mark operator
  (blender/blender@dd662c74ae).
- Fix unreported: Fix possible invalid normal use w/ tangent calc
  (blender/blender@d252ac6b95).
- Fix unreported: Fix error accessing tessface color in edit-mode
  (blender/blender@a033a7bef9).
- Fix unreported: Fix freeing all custom-data layers
  (blender/blender@ac66fb193f).
- Fix unreported: Fix/workaround 'convert object' messing up linked data
  (blender/blender@880e96dd66).
- Fix unreported: Fix topology mirror ignoring center verts
  (blender/blender@6e90294e08).
- \[Release\] Fix unreported: Fix minor Mesh -\> BMesh conversion issues
  (blender/blender@6fec06926e).

### Meta Editing

- \[RC2\] Fix [\#52324](http://developer.blender.org/T52324): Metaball
  disappears when deleting first metaball object
  (blender/blender@ff47118c73).

### Modifiers

- Fix [\#50216](http://developer.blender.org/T50216): Missing checks
  caused data transfer segfault
  (blender/blender@2e15618f49).
- Fix [\#50373](http://developer.blender.org/T50373): lattices should
  not be able to get subsurf modifiers
  (blender/blender@378afc9830).
- Fix [\#50534](http://developer.blender.org/T50534): Part II: warn user
  when DataTransfer mod affects custom normals
  (blender/blender@11abb13483).
- Fix [\#50830](http://developer.blender.org/T50830): Wrong context when
  calling surfacedeform_bind
  (blender/blender@856077618a).
- Fix [\#50838](http://developer.blender.org/T50838): Surface Deform DM
  use after free issue
  (blender/blender@2089a17f7e).
- Fix [\#50899](http://developer.blender.org/T50899): Alternate
  (blender/blender@2fbc50e4c1).
- Fix [\#51012](http://developer.blender.org/T51012): Surface modifier
  was not working with curves
  (blender/blender@13d8661503).
- Fix [\#50851](http://developer.blender.org/T50851): Array modifier
  generating invalid geometry
  (blender/blender@4d58080e23).
- Fix [\#51890](http://developer.blender.org/T51890): Turning off
  viewport display for array modifier is blocking dupliface in final
  render
  (blender/blender@0210c6b9de).
- Fix [\#52212](http://developer.blender.org/T52212): Vgroups doesn't
  work after Carve Boolean
  (blender/blender@d41acacc61).
- \[RC2\] Fix [\#51701](http://developer.blender.org/T51701): Alembic
  cache screws up mesh
  (blender/blender@8138082ab8).
- \[Release\] Fix [\#52478](http://developer.blender.org/T52478): Error
  report "Shrinkwrap: out of memory" on invisible target
  (blender/blender@a5213924a8).
- \[Release\] Fix [\#52149](http://developer.blender.org/T52149):
  LoopTriArray computation was not correctly protected against
  concurrency
  (blender/blender@9ae35fafb6).

<!-- -->

- Fix unreported: Fix potential NULL dereference in mesh sequence cache
  modifier
  (blender/blender@841c4deed7).
- Fix unreported: Fix (IRC reported) DataTransfer modifier affecting
  base mesh in some cases
  (blender/blender@e4e1900012).
- Fix unreported: Fix Surface Deform crash with missing or freed DM
  (blender/blender@0a032ce83b).
- Fix unreported: Fix Surface Deform not unbinding if target is removed
  (blender/blender@ca958642fa).
- Fix unreported: Fix (unreported) memleak in Warp modifier copying
  (blender/blender@5b6838129f).
- Fix unreported: Fix integer overflows in meshcache modifier
  (blender/blender@bddd9d809d).
- Fix unreported: Fix (unreported) Dynamic Paint modifier not increasing
  ID usercount in copy function
  (blender/blender@0d5c7e5e36).
- Fix unreported: Fix (unreported) bad copying code in Mesh Deform
  modifier
  (blender/blender@e917bc5ee0).
- Fix unreported: Fix (unreported) bad copying of Ocean modifier
  (blender/blender@1addac8e0c).
- Fix unreported: Fix (unreported) bad copying code of Surface Deform
  modifier
  (blender/blender@24486513d5).
- Fix unreported: Opensubdiv: Fix compilation error with older
  Opensubdiv versions
  (blender/blender@1dfc4be6ab).

### Material / Texture

- Fix [\#50590](http://developer.blender.org/T50590): BI lamp doesn't
  hold a texture in this case
  (blender/blender@6663099810).
- Fix [\#51913](http://developer.blender.org/T51913): Context tab for
  textures issue
  (blender/blender@f11bcbed9d).
- Fix [\#51746](http://developer.blender.org/T51746): normal map
  tangents not working correctly when there are no UV maps
  (blender/blender@e3e5ecfaa8).
- Fix [\#51951](http://developer.blender.org/T51951): cell noise texture
  precision issue at unit vertex coordinates
  (blender/blender@3232d8ec8f).
- Fix [\#51734](http://developer.blender.org/T51734): batch-generate
  previews crashes on some materials
  (blender/blender@0c122d64d2).
- Fix [\#52034](http://developer.blender.org/T52034): cell noise renders
  different
  (blender/blender@5c30bc285c).

<!-- -->

- Fix unreported: Fix reading past bounds removing from color ramp
  (blender/blender@57bcc19bb3).
- Fix unreported: Fix (unreported) memory leak in Fluid modifier copying
  (blender/blender@e9aaf5ed21).

## Physics / Simulations / Sculpt / Paint

### Particles

- Fix [\#51390](http://developer.blender.org/T51390): Blender 2.78c will
  freeze or force close when adding particles at random
  (blender/blender@06ca2c9d03).
- Fix [\#51523](http://developer.blender.org/T51523): Lattice modifier
  affecting particles even when disabled
  (blender/blender@56422ff6c3).
- Fix [\#51991](http://developer.blender.org/T51991): Disabled for
  viewport particle system becomes visible after BI render
  (blender/blender@4d4ebc8300).

### Physics / Hair / Simulations

- Fix [\#50141](http://developer.blender.org/T50141): Nabla zero
  division on texture force field
  (blender/blender@b18f83bcf4).
- Fix [\#50350](http://developer.blender.org/T50350): Quick Explode time
  frame problem
  (blender/blender@c0c48cdacc).
- Fix [\#50056](http://developer.blender.org/T50056): Dyntopo brush size
  shortcut broken using constant detail setting
  (blender/blender@092cbcd1d2).
- Fix [\#50634](http://developer.blender.org/T50634): Hair Primitive as
  Triangles + Hair shader with a texture = crash
  (blender/blender@f1b21d5960).
- Fix [\#51176](http://developer.blender.org/T51176): Cache file
  location can be blank and prevent fluid simulation from reading baked
  data
  (blender/blender@4621392353).
- Fix [\#45350](http://developer.blender.org/T45350): Cache not being
  recomputed with "Bake All Dynamics"
  (blender/blender@ee3faddfaa).
- Fix [\#51297](http://developer.blender.org/T51297): Use A Higher Hair
  Segment Limit
  (blender/blender@4621583612).
- Fix [\#51524](http://developer.blender.org/T51524): Instantiated Hair
  Object which has dupligroup children and hidden orig objects of group
  crash at render
  (blender/blender@b6f5e8d9a1).
- Fix [\#51774](http://developer.blender.org/T51774): Children particles
  hair interpolation not correct with textures or dp
  (blender/blender@3190eaf109).
- Fix [\#50887](http://developer.blender.org/T50887): Holes in fluid
  mesh on Windows
  (blender/blender@9c2bbfb6ce).
- Fix [\#50230](http://developer.blender.org/T50230): Rigid Body
  simulation shouldn't step when time is beyond cached area
  (blender/blender@9cd6b03187).
- Fix [\#51759](http://developer.blender.org/T51759): fluid simulation
  particles not remoevd when colliding with outflow objects
  (blender/blender@405121d613).
- Fix [\#51703](http://developer.blender.org/T51703): Rigid body with
  delta transform jumps when transforming
  (blender/blender@d1dfd5fa26).
- Fix [\#51296](http://developer.blender.org/T51296): UVs not working
  for hair emitted from vertices
  (blender/blender@fed853ea78).
- Fix [\#51977](http://developer.blender.org/T51977): New depsgraph
  removes the grass in victor scene
  (blender/blender@1e21778261).
- Fix [\#52156](http://developer.blender.org/T52156): Hair dynamics
  broken with density texture
  (blender/blender@9b22dbcc0d).
- \[RC2\] Fix [\#52344](http://developer.blender.org/T52344): Softbody
  on Text
  (blender/blender@efa840f99f).
- \[RC2\] Fix [\#52344](http://developer.blender.org/T52344): Softbody
  on Text
  (blender/blender@9a239eea68).
- \[Release\] Fix [\#52439](http://developer.blender.org/T52439): Crash
  after adjusting lenght of hair particles
  (blender/blender@d2f20aed04).
- \[Release\] Fix [\#52498](http://developer.blender.org/T52498):
  Deleting force field doesn't remove "Surface" from modifier stack
  (blender/blender@82a6889d83).
- \[Release\] Fix [\#52374](http://developer.blender.org/T52374):
  Changes of rigid body related settings during simulation will break
  the simulation
  (blender/blender@d84f559555).

<!-- -->

- Fix unreported: Viewport smoke: fix a couple of issues in the new
  display settings
  (blender/blender@ae69986b70).
- Fix unreported: Fix memory leak when Blender is build without Bullet
  and files with RB is opened
  (blender/blender@0085001eb0).
- Fix unreported: Dynamic Paint: Fix random pixel flooding by absolute
  brush with spread
  (blender/blender@b86042f21a).
- Fix unreported: Fix missing hair after rendering with different
  viewport/render settings
  (blender/blender@03544eccb4).
- Fix unreported: Fix part of
  [\#50634](http://developer.blender.org/T50634): Hair Primitive as
  Triangles + Hair shader with a texture = crash
  (blender/blender@209a64111e).
- Fix unreported: Fix hair_step is a short
  (blender/blender@12651aba03).
- Fix unreported: Fix rigid body not resimulating after cache
  invalidation
  (blender/blender@3edc8c1f9b).
- \[Release\] Fix unreported: Rigidbody: Fix regression introduced in
  ee3fadd
  (blender/blender@e91f9f664d).

### Sculpting / Painting

- Fix [\#50035](http://developer.blender.org/T50035): Minor interface
  bug: UV/ImageEditor - Paint Mode - Fill Brush
  (blender/blender@27de0c40c5).
- Fix [\#51761](http://developer.blender.org/T51761): wpaint select
  depth limit fails
  (blender/blender@a247b367b0).
- Fix [\#46560](http://developer.blender.org/T46560): 2D paint smear and
  soften brushes not working with alpha
  (blender/blender@0d4fd7528f).
- Fix [\#50039](http://developer.blender.org/T50039): texture paint
  soften strength not working with float images
  (blender/blender@b379f02f20).
- \[Release\] Fix [\#52639](http://developer.blender.org/T52639): Weight
  paint smooth tool crash
  (blender/blender@300abf241e).
- \[Release\] Fix [\#52696](http://developer.blender.org/T52696):
  Sculpt - Brush spacing pressure artifacts
  (blender/blender@9a9e9b1c4d).

<!-- -->

- Fix unreported: Fix (unreported) bad handling of brush's fill
  threshold value
  (blender/blender@f6083b7bcd).
- Fix unreported: Fix brush menu broken before adding uv and texture
  data
  (blender/blender@157a8727b5).
- Fix unreported: Fix texture paint crash when painting onto stencil
  (blender/blender@248bba81e7).

## Image / Video / Render

### Image / UV Editing

- Fix [\#49343](http://developer.blender.org/T49343):
  ImageFormatSettings were not setting their rna struct path correctly
  (blender/blender@f64aa4e0af).
- Fix [\#49391](http://developer.blender.org/T49391): Texture paint is
  not aware of disabled color management
  (blender/blender@075a2175d5).
- Fix [\#49815](http://developer.blender.org/T49815): Blender always
  reverts to RGBA when using Save As Image
  (blender/blender@a39ab9cfde).
- Fix [\#50071](http://developer.blender.org/T50071): Radience HDR fomat
  does not support alpha at all
  (blender/blender@b6c0edcb09).
- Fix [\#50020](http://developer.blender.org/T50020): adding a
  background image does not set image user data
  (blender/blender@8ce6de3bdd).
- Fix [\#50122](http://developer.blender.org/T50122): SEGFAULT: OCIO
  configuration typo leads to segfault
  (blender/blender@1b9cae9d04).
- Fix [\#50115](http://developer.blender.org/T50115): stereoscopic video
  file memory leak
  (blender/blender@e5482f986c).
- Fix [\#51187](http://developer.blender.org/T51187): Memory leak when
  exporting OpenEXR monochrome animation
  (blender/blender@2836003f6b).
- Fix [\#51153](http://developer.blender.org/T51153): Video duration is
  detected wrong after FFmpeg update
  (blender/blender@8ca9fa5fd3).
- Fix [\#50673](http://developer.blender.org/T50673): OpenEXR multilayer
  image truncated on save
  (blender/blender@d59721c2c3).
- Fix [\#51609](http://developer.blender.org/T51609): Bake Texture,
  Margin crashing Blender
  (blender/blender@bf5e717ef5).
- Fix [\#51587](http://developer.blender.org/T51587): Blender fails to
  interpret a specific layer in OpenEXR multilayer file
  (blender/blender@556942ec07).
- \[RC2\] Fix [\#52334](http://developer.blender.org/T52334): images
  with non-color data should not change color space on save
  (blender/blender@35f5d80f3a).

<!-- -->

- Fix unreported: Dynamic Paint: Fix adjacency computations at UV seams
  (blender/blender@9a66d0ad1b).
- Fix unreported: FFmpeg: Fix off by one error in number of detected
  frames in matroska container
  (blender/blender@a54242503e).
- Fix unreported: Fix (unreported) memleak in ImBuf mipmap code in some
  cases
  (blender/blender@a4c6558481).
- Fix unreported: Fix (unreported) crash in 'Match movie length
  operator' in case of lost video texture file
  (blender/blender@b47c912f4b).
- Fix unreported: Image: Fix non-deterministic behavior of image
  sequence loading
  (blender/blender@e76364adcd).
- Fix unreported: Fix (unreported): Crash if a right click is performed
  on an image datablock (open btn f.e.)
  (blender/blender@894513c7fd).
- Fix unreported: Fix memory leak when saving OpenEXR files
  (blender/blender@26b2323189).
- Fix unreported: Fix crash from freeing of NULL pointer
  (blender/blender@0a6c57d3d2).
- Fix unreported: Partial fix of
  [\#51989](http://developer.blender.org/T51989): Don't set image user
  offset for movie files
  (blender/blender@9a4cfc3e77).

### Masking

- Fix [\#50062](http://developer.blender.org/T50062): Mask - Clicking in
  ActivePoint Parent makes Blender crash
  (blender/blender@0c958b9f8e).
- Fix [\#51388](http://developer.blender.org/T51388): Mask moves when
  zoom is changed in the movie clip editor
  (blender/blender@47f8459ead).

<!-- -->

- Fix unreported: Masks: Fix broken animation after adding primitives
  (blender/blender@294ffa0d49).
- Fix unreported: Fix (unreported) missing Image usercount increase when
  copying UVProject modifier
  (blender/blender@e3b1d562a7).

### Motion Tracking

- Fix [\#51158](http://developer.blender.org/T51158): Motion Tracking
  Movie clip editor graph
  (blender/blender@a40f15d04f).
- Fix [\#50908](http://developer.blender.org/T50908): Motion Tracker
  ignored grease pencil mask
  (blender/blender@b0015686e2).
- Fix [\#51980](http://developer.blender.org/T51980): Motion Tracking -
  png image files appear in the Blender program directory when using
  refine
  (blender/blender@aaec4ed07d).
- Fix [\#51978](http://developer.blender.org/T51978): Setup Tracking
  Scene after Motion Tracking fails the first time
  (blender/blender@d25ccf83ad).

<!-- -->

- Fix unreported: Libmv: Fix copy-paste mistake in camera intrinsic
  parameters
  (blender/blender@356dacea90).
- Fix unreported: Libmv: Fix missing virtual destructor in frame access
  sub-class
  (blender/blender@1c34a7f4eb).
- Fix unreported: Fix float buffer of tracking image accessed outside of
  check that it has been correctly allocated
  (blender/blender@21e1282265).
- Fix unreported: Libmv: Fix crash of keyframe selection on 32bit linux
  (blender/blender@a1f8755d32).
- Fix unreported: Tracking: Fix use-after-free bug
  (blender/blender@58f3b3c6d1).

### Nodes / Compositor

- Fix [\#49857](http://developer.blender.org/T49857): Blender crashes
  after adding texture node to compositing tree
  (blender/blender@534f11f71e).
- Fix [\#50736](http://developer.blender.org/T50736): Zero streaks in
  Glare node
  (blender/blender@9dd194716b).
- Fix [\#50656](http://developer.blender.org/T50656): Compositing node
  editor is empty, no nodes can be added
  (blender/blender@9eb647f1c8).
- Fix [\#50849](http://developer.blender.org/T50849): Transparent
  background produces artifacts in this compositing setup
  (blender/blender@817e975dee).
- Fix [\#51455](http://developer.blender.org/T51455): Render Layers in
  compositor from a different scene not working
  (blender/blender@b82954f6f4).
- Fix [\#51449](http://developer.blender.org/T51449): empty node editor
  continuously redrawing and using CPU
  (blender/blender@868678c85f).
- Fix [\#51348](http://developer.blender.org/T51348): Node highlighting
  is broken
  (blender/blender@1f96dd2e0b).
- Fix [\#51308](http://developer.blender.org/T51308): Bright/Contrast
  Doesn't respect Pre-multiplied Alpha
  (blender/blender@8cc4c3da8c).
- Fix [\#51687](http://developer.blender.org/T51687): GPUmat evaluation
  of shader tree would crash uppon unknown/unsupported socket types
  (blender/blender@528ae8885e).
- Fix [\#51840](http://developer.blender.org/T51840): UI redraw in node
  editor header missing on pointcache bake
  (blender/blender@f158a206f2).
- Fix [\#51863](http://developer.blender.org/T51863):
  CompositorNodeSwitchView have the wrong rna API
  (blender/blender@be4b5551c7).
- Fix [\#52092](http://developer.blender.org/T52092): Crash un-grouping
  nodes
  (blender/blender@3daa641d7f).
- Fix [\#52232](http://developer.blender.org/T52232): Crash in
  RNA_enum_from_value while inspecting compositor image node properties
  in outliner
  (blender/blender@f815aa01eb).
- \[RC2\] Fix [\#52280](http://developer.blender.org/T52280): The Image
  node in Compositing can't read Z buffer of openEXR in 2.79
  (blender/blender@e54df78c82).
- \[Release\] Fix [\#52218](http://developer.blender.org/T52218):
  Missing update when reconnecting node
  (blender/blender@f2aa9bec9d).
- \[Release\] Fix [\#52531](http://developer.blender.org/T52531):
  Blender 2D stabilisation node issue when autoscale is selected
  (blender/blender@82466852fe).

<!-- -->

- Fix unreported: Fix Node space ID remap callback not handling node
  trees
  (blender/blender@884693b42a).
- Fix unreported: Fix (unreported) nodeRemoveAllSockets() not clearing
  inputs/outputs sockets lists
  (blender/blender@bd6a9fd734).
- Fix unreported: Fix NodeGroup generic verify function crashing if
  node's ID pointer is NULL
  (blender/blender@fa6a62fac2).
- Fix unreported: Fix/workaround
  [\#51007](http://developer.blender.org/T51007): Material viewport mode
  crash on node with more than 64 outputs
  (blender/blender@6aa972ebd4).
- Fix unreported: Fix: [\#50271](http://developer.blender.org/T50271):
  Bilateral/Directional blur's iterations is zero by default
  (blender/blender@a85f457195).
- Fix unreported: Fix error in node flag check
  (blender/blender@6abcd6cfd5).
- Fix unreported: Fix/workaround
  [\#51070](http://developer.blender.org/T51070): Cannot scale
  procedural texture in compositor
  (blender/blender@ec051f5103).
- Fix unreported: Fix unreported: Copy-pasting nodes crashes when they
  have an undefined type
  (blender/blender@cd8c46627f).
- Fix unreported: Compositor: Fix compilation error and crash when using
  defocus search
  (blender/blender@e3bddcb215).
- Fix unreported: Fix memory leak caused by node clipboard
  (blender/blender@d5d7d453a5).
- Fix unreported: Fix compositor Glare node with Simpler Star resulting
  in uneven rays
  (blender/blender@b7fb00f512).

### Render

- Fix [\#49393](http://developer.blender.org/T49393): Baking ignores
  backfaces
  (blender/blender@8ced4417f9).
- Fix [\#49937](http://developer.blender.org/T49937): Blender is
  crashing because of Lamp Data Node
  (blender/blender@508e2f0d69).
- Fix [\#50094](http://developer.blender.org/T50094): Crash when
  viewport rendering point density texture
  (blender/blender@1ec5edcc96).
- Fix [\#50032](http://developer.blender.org/T50032): Wrong render
  result when same image is used with and without alpha
  (blender/blender@ced20b74e5).
- Fix [\#50542](http://developer.blender.org/T50542): Wrong metadata
  frame when using OpenGL render
  (blender/blender@13d31b1604).
- Fix [\#49429](http://developer.blender.org/T49429): incorrect Blender
  internal viewport border render with DrawPixels method
  (blender/blender@6fc7521ade).
- Fix [\#50109](http://developer.blender.org/T50109): Blender crash when
  a "Render Result" as a Texture
  (blender/blender@1c21e088f2).
- Fix [\#49864](http://developer.blender.org/T49864): EnvMap baking
  crashes 2.78 if 'Full Sample' checked in AA
  (blender/blender@a70a7f9db3).
- Fix [\#52011](http://developer.blender.org/T52011): Border render is
  incorrect with multiple scenes
  (blender/blender@440c91b1f6).
- Fix [\#52116](http://developer.blender.org/T52116): Blender internal
  BVH build crash in degenerate cases
  (blender/blender@5376b3eeca).
- \[Release\] Fix [\#52473](http://developer.blender.org/T52473):
  blender internal Fresnel and Layer Weight only work with linked normal
  (blender/blender@dd84324485).

<!-- -->

- Fix unreported: OpenGL render: Bugfix (unreported)
  afterblender/blender@6f92604e539b2114763150fb1ace60d28e59a889
  (blender/blender@cade262c47).
- Fix unreported: OpenGL render: Fix non-deterministic order of frame
  writes for movies
  (blender/blender@ae2471b850).
- Fix unreported: OpenGL render: Fix missing file output after pressing
  Esc
  (blender/blender@54dad5c49f).
- Fix unreported: Fix World Space Shading option influence on Fresnel
  node for BI + cleanup
  (blender/blender@84a283d18c).
- Fix unreported: Fix unreported bug in Blender Render: using
  unnormalized normal in normal map node in the same way as in baking
  (blender/blender@27d20a04b5).
- Fix unreported: Fix threading conflicts in multitex_ext_safe()
  (blender/blender@25ab3aac9d).
- Fix unreported: Fix missing render update when building without OCIO
  but having GLSL image draw method
  (blender/blender@717d85fb1c).
- Fix unreported: Fix memory leak in environment
  (blender/blender@f89c6e739a).

### Render: Cycles

- Fix [\#49630](http://developer.blender.org/T49630): Cycles: Swapped
  shader and bake kernels
  (blender/blender@cd843409d3).
- Fix [\#49793](http://developer.blender.org/T49793): Fix enabling SSE2
  globally for msvc
  (blender/blender@789ea7397f).
- Fix [\#49846](http://developer.blender.org/T49846): OpenCL rendering
  compilation failure
  (blender/blender@5aa6a2ec06).
- Fix [\#49901](http://developer.blender.org/T49901): Cycles: OpenCL
  build error after recent light texture coordinate commit
  (blender/blender@9847ad977a).
- Fix [\#49952](http://developer.blender.org/T49952): Cycles: Bad MIS
  sampling of backgrounds with single bright pixels
  (blender/blender@f89fbf580e).
- Fix [\#49838](http://developer.blender.org/T49838): Noise
  randomization for frame should be done per interframes as well
  (blender/blender@a2d78d7a46).
- Fix [\#49985](http://developer.blender.org/T49985): cycles standalone
  using wrong socket names for XML reading
  (blender/blender@188ecee642).
- Fix [\#49985](http://developer.blender.org/T49985): cycles standalone
  XML missing distant lights
  (blender/blender@478e59a04e).
- Fix [\#49904](http://developer.blender.org/T49904): Cycles standalone
  missing default generated texture coordinates
  (blender/blender@111e2f5aba).
- Fix [\#50001](http://developer.blender.org/T50001): auto tile size
  addon broken after Cycles GPU device changes
  (blender/blender@60409841a4).
- Fix [\#50100](http://developer.blender.org/T50100): Cycles
  SeparateRGBNode Red socket defined wrong
  (blender/blender@def365e252).
- Fix [\#50104](http://developer.blender.org/T50104): Race condition in
  SVMShaderManager::device_update_shader
  (blender/blender@265e5def76).
- Fix [\#50116](http://developer.blender.org/T50116): Light threshold
  broke branched path tracer
  (blender/blender@f812b05922).
- Fix [\#50460](http://developer.blender.org/T50460): . Greying out
  issue with Cycles culling options
  (blender/blender@ff1b850081).
- Fix [\#50491](http://developer.blender.org/T50491): Cycles UI breaks
  when pushing F8
  (blender/blender@ce8889175a).
- Fix [\#49405](http://developer.blender.org/T49405): Crash when baking
  with adaptive subdivision
  (blender/blender@a7d5cabd4e).
- Fix [\#50517](http://developer.blender.org/T50517): Rendering
  expecting time is negative
  (blender/blender@8ea09252c8).
- Fix [\#50535](http://developer.blender.org/T50535): Cycles render
  segfault when Explode modifier before hair particle modifier + UV
  material
  (blender/blender@86747ff180).
- Fix [\#49253](http://developer.blender.org/T49253): Cycles blackbody
  is wrong on AVX2 CPU on Windows
  (blender/blender@53896d4235).
- Fix [\#50655](http://developer.blender.org/T50655): Pointiness is too
  slow to calculate
  (blender/blender@37afa965a4).
- Fix [\#50687](http://developer.blender.org/T50687): Cycles baking time
  estimate and progress bar doesn't work / progress when baking with
  high samples
  (blender/blender@306acb7dda).
- Fix [\#50719](http://developer.blender.org/T50719): Memory usage won't
  reset to zero while re-rendering on two video cards
  (blender/blender@333dc8d60f).
- Fix [\#50718](http://developer.blender.org/T50718): Regression: Split
  Normals Render Problem with Cycles
  (blender/blender@696836af1d).
- Fix [\#50748](http://developer.blender.org/T50748): Render Time
  incorrect when refreshing rendered preview in GPU mode
  (blender/blender@60592f6778).
- Fix [\#50698](http://developer.blender.org/T50698): Cycles baking
  artifacts with transparent surfaces
  (blender/blender@8c5826f59a).
- Fix [\#49936](http://developer.blender.org/T49936): Cycles point
  density get's it's bounding box from basis shape key
  (blender/blender@a581b65822).
- Fix [\#49603](http://developer.blender.org/T49603): Blender/Cycles
  2.78 CUDA error on Jetson-TX1~
  (blender/blender@05dfe9c318).
- Fix [\#50888](http://developer.blender.org/T50888): Numeric overflow
  in split kernel state buffer size calculation
  (blender/blender@96868a3941).
- Fix [\#50628](http://developer.blender.org/T50628): gray out cycles
  device menu when no device configured only for GPU Compute
  (blender/blender@68ca973f7f).
- Fix [\#50925](http://developer.blender.org/T50925): Add AO
  approximation to split kernel
  (blender/blender@f169ff8b88).
- Fix [\#50876](http://developer.blender.org/T50876): Cycles Crash -
  Cycles crashes before sampling when certain meshes have autosmooth
  enabled
  (blender/blender@1410ea0478).
- Fix [\#50968](http://developer.blender.org/T50968): Cycles crashes
  when image datablock points to a directory
  (blender/blender@ea3d7a7f58).
- Fix [\#50990](http://developer.blender.org/T50990): Random black
  pixels in Cycles when rendering material with Multiscatter GGX
  (blender/blender@18bf900b31).
- Fix [\#50975](http://developer.blender.org/T50975): Cycles: Light
  sampling threshold inadvertently clamps negative lamps
  (blender/blender@a201b99c5a).
- Fix [\#50268](http://developer.blender.org/T50268): Cycles allows to
  select un supported GPUs for OpenCL
  (blender/blender@3c4df13924).
- Fix [\#50238](http://developer.blender.org/T50238): Cycles: difference
  in texture position between OpenGL and Cycles render
  (blender/blender@467d824f80).
- Fix [\#51051](http://developer.blender.org/T51051): Incorrect render
  on 32bit Linux
  (blender/blender@ced8fff5de).
- Fix [\#51115](http://developer.blender.org/T51115): Bump node is
  broken when the displacement socket is used
  (blender/blender@ab347c8380).
- Fix [\#51412](http://developer.blender.org/T51412): Instant crash with
  texture plugged into the Displacement output
  (blender/blender@a523dfd2fd).
- Fix [\#51314](http://developer.blender.org/T51314): crash cancelling
  Cycles bake during scene sync and update
  (blender/blender@890d871bc3).
- Fix [\#51501](http://developer.blender.org/T51501): Cycles baking
  cancel affects baking script
  (blender/blender@b60f80e9b3).
- Fix [\#49324](http://developer.blender.org/T49324): True displacement
  crashes when shader returns NaN
  (blender/blender@ef8ad66aa2).
- Fix [\#50937](http://developer.blender.org/T50937): baking with OpenCL
  and CPU have slightly different brightness
  (blender/blender@40e6f65ea1).
- Fix [\#51529](http://developer.blender.org/T51529): Black boxes on a
  denoising render when using a .exr image as a environmental texture
  (blender/blender@1d49205b1a).
- Fix [\#51408](http://developer.blender.org/T51408): Cycles -
  Principled BSDF Shader - Transparency is not working as expected
  (blender/blender@32c9d2322c).
- Fix [\#51555](http://developer.blender.org/T51555): Cycles tile count
  is incorrect when denoising is enabled
  (blender/blender@a21277b996).
- Fix [\#51506](http://developer.blender.org/T51506): Wrong shadow
  catcher color when using selective denoising
  (blender/blender@cf1127f380).
- Fix [\#51553](http://developer.blender.org/T51553): Cycles Volume
  Emission turns black when strength is 0 or color is black
  (blender/blender@4a04d7ae89).
- Fix [\#51537](http://developer.blender.org/T51537): Light passes are
  summed twice for split kernel since denoise commit
  (blender/blender@8e655446d1).
- Fix [\#51560](http://developer.blender.org/T51560): Black pixels on a
  denoising render
  (blender/blender@3dee1f079f).
- Fix [\#51568](http://developer.blender.org/T51568): CUDA error in
  viewport render after fix for for OpenCL
  (blender/blender@34b689892b).
- Fix [\#51592](http://developer.blender.org/T51592): Simplify AO Cycles
  setting remains active while Simplify is disabled
  (blender/blender@7add6b89bc).
- Fix [\#51589](http://developer.blender.org/T51589): Principled
  Subsurface Scattering, wrong shadow color
  (blender/blender@e20a33b89d).
- Fix [\#51652](http://developer.blender.org/T51652): Cycles -
  Persistant Images not storing images
  (blender/blender@9b914764a9).
- Fix [\#49570](http://developer.blender.org/T49570): Cycles baking
  can't handle materials with no images
  (blender/blender@2cd7b80cae).
- Fix [\#51791](http://developer.blender.org/T51791): Point Density
  doesn't work on GPU
  (blender/blender@6cfa3ecd4d).
- Fix [\#51849](http://developer.blender.org/T51849): change Cycles
  clearcoat gloss to roughness
  (blender/blender@14ea0c5fcc).
- Fix [\#51836](http://developer.blender.org/T51836): Cycles: Fix
  incorrect PDF approximations of the MultiGGX closures
  (blender/blender@8cb741a598).
- Fix [\#51909](http://developer.blender.org/T51909): Cycles:
  Uninitialized closure normals for the Hair BSDF
  (blender/blender@1f3fd8e60a).
- Fix [\#51957](http://developer.blender.org/T51957): principled BSDF
  mismatches in GLSL viewport
  (blender/blender@eb420e6b7f).
- Fix [\#51956](http://developer.blender.org/T51956): color noise with
  principled sss, radius 0 and branched path
  (blender/blender@29c8c50442).
- Fix [\#51855](http://developer.blender.org/T51855): Cycles emssive
  objects with NaN transform break lighting
  (blender/blender@cda24d0853).
- Fix [\#51950](http://developer.blender.org/T51950): Abnormally long
  Cycles OpenCL GPU render times with certain panoramic camera settings
  (blender/blender@15fd758bd6).
- Fix [\#51967](http://developer.blender.org/T51967): OSL crash after
  rendering finished (mainly on Windows)
  (blender/blender@3361f2107b).
- Fix [\#52001](http://developer.blender.org/T52001): material draw mode
  principled BSDF artifacts at some angles
  (blender/blender@ba256b32ee).
- Fix [\#52027](http://developer.blender.org/T52027): OSL getattribute()
  crash, when optimizer calls it before rendering
  (blender/blender@29ec0b1162).
- Fix [\#52021](http://developer.blender.org/T52021): Shadow catcher
  renders wrong when catcher object is behind transparent object
  (blender/blender@5f35682f3a).
- Fix [\#52107](http://developer.blender.org/T52107): Color management
  difference when using multiple and different GPUs together
  (blender/blender@4bc6faf9c8).
- Fix [\#52125](http://developer.blender.org/T52125): principled BSDF
  missing with macOS OpenCL
  (blender/blender@3b12a71972).
- Fix [\#52135](http://developer.blender.org/T52135): Cycles should not
  keep generated/packed images in memory after render
  (blender/blender@2b132fc3f7).
- Fix [\#52152](http://developer.blender.org/T52152): allow zero
  roughness for Cycles principled BSDF, don't clamp
  (blender/blender@e982ebd6d4).
- Fix [\#51450](http://developer.blender.org/T51450): viewport render
  time keeps increasing after render is done
  (blender/blender@e93804318f).
- \[Release\] Fix [\#51805](http://developer.blender.org/T51805):
  Overlapping volumes renders incorrect on AMD GPU
  (blender/blender@6825439b36).
- \[Release\] Fix [\#52533](http://developer.blender.org/T52533):
  Blender shuts down when rendering duplicated smoke domain
  (blender/blender@c9d653e560).

<!-- -->

- Fix unreported: Cycles: Fix another OpenCL logging issue
  (blender/blender@f7ce482385).
- Fix unreported: Cycles: Fix OpenCL compilation with the new brick
  texture
  (blender/blender@2e9dd1200f).
- Fix unreported: Cycles: Fix OpenCL build error caused by light
  termination commit
  (blender/blender@f800794b97).
- Fix unreported: Fix Brick Texture GLSL, broken after Mortar Smooth
  addition
  (blender/blender@28639a22bc).
- Fix unreported: Fix Cycles OSL compilation based on modified time not
  working
  (blender/blender@b5a58507f2).
- Fix unreported: Cycles: Fix different noise pattern from fix in
  [\#49838](http://developer.blender.org/T49838)
  (blender/blender@2a2eb0c463).
- Fix unreported: Fix Cycles device backwards compatibility error if
  device type is unavailable
  (blender/blender@411836d97c).
- Fix unreported: Cycles: Fix correlation issues in certain cases
  (blender/blender@9d50175b6c).
- Fix unreported: Fix emissive volumes generates unexpected fireflies
  around intersections
  (blender/blender@dd58390d71).
- Fix unreported: Cycles: Fix wrong motion blur when combining
  deformation motion blur with autosplit
  (blender/blender@394fa07d41).
- Fix unreported: Cycles: Fix wrong scaling of traversed instances debug
  pass
  (blender/blender@789fdab825).
- Fix unreported: Cycles: Fix wrong transparent shadows for motion blur
  hair
  (blender/blender@e5a665fe24).
- Fix unreported: Cycles: Fix amount of rendered samples not being shown
  while rendering the last tile on CPU
  (blender/blender@4a19112277).
- Fix unreported: Cycles: Fix typo in the panel name
  (blender/blender@77982e159c).
- Fix unreported: Cycles: Fix rng_state initialization when using
  resumable rendering
  (blender/blender@fa19940dc6).
- Fix unreported: Cycles: Fix regression with transparent shadows in
  volume
  (blender/blender@b16fd22018).
- Fix unreported: Cycles: Fix pointiness attribute giving wrong results
  with autosplit
  (blender/blender@fd7e9f7974).
- Fix unreported: Cycles: Fix wrong shading on GPU when background has
  NaN pixels and MIS enabled
  (blender/blender@581c819013).
- Fix unreported: Cycles: Fix wrong transparent shadows with CUDA
  (blender/blender@21dbfb7828).
- Fix unreported: Cycles: Fix wrong pointiness caused by precision
  issues
  (blender/blender@5723aa8c02).
- Fix unreported: Cycles: Fix missing initialization of triangle BVH
  steps
  (blender/blender@088c6a17ba).
- Fix unreported: Cycles: Fix wrong hair render results when using BVH
  motion steps
  (blender/blender@dc7bbd731a).
- Fix unreported: Cycles: Fix shading with autosmooth and custom normals
  (blender/blender@36c4fc1ea9).
- Fix unreported: Fix Cycles still saving render output when error
  happened
  (blender/blender@75cc33fa20).
- Fix unreported: Cycles: Fix wrong render results with texture limit
  and half-float textures
  (blender/blender@4e12113bea).
- Fix unreported: Cycles: Fix non-zero exit status when rendering
  animation from CLI and running out of memory
  (blender/blender@f49e28bae7).
- Fix unreported: Fix/workaround
  [\#48549](http://developer.blender.org/T48549): Crash baking
  high-to-low-poly normal map in cycles
  (blender/blender@efe78d824e).
- Fix unreported: Cycles: Fix division by zero in volume code which was
  producing -nan
  (blender/blender@87f236cd10).
- Fix unreported: Cycles: Fix possibly uninitialized variable
  (blender/blender@810d7d4694).
- Fix unreported: Cycles: Fix crash after failed kernel build
  (blender/blender@997e345bd2).
- Fix unreported: Cycles: Fix CUDA build error for some compilers
  (blender/blender@c837bd5ea5).
- Fix unreported: Cycles: Fix handling of barriers
  (blender/blender@60a344b43d).
- Fix unreported: Cycles: Fix wrong vector allocation in the mesh sync
  code
  (blender/blender@eb1a57b12c).
- Fix unreported: Fix/workaround
  [\#50533](http://developer.blender.org/T50533): Transparency shader
  doesn't cast shadows with curve segments
  (blender/blender@2b44db4cfc).
- Fix unreported: Cycles: Fix speed regression on GPU
  (blender/blender@a1348dde2e).
- Fix unreported: Cycles: Fix uninitialized memory access when comparing
  curve mapping nodes
  (blender/blender@5ce95df2c6).
- Fix unreported: Cycles: Fix race condition in attributes creation
  during SVM compilation
  (blender/blender@52029e689c).
- Fix unreported: Cycles: Fix corrupted mesh render when topology
  differs at the next frame
  (blender/blender@9706bfd25d).
- Fix unreported: Cycles: Fix access of NULL pointer as array
  (blender/blender@fd08570665).
- Fix unreported: Cycles: Fix race condition in shader attribute for
  real now
  (blender/blender@c8e764ccbf).
- Fix unreported: Cycles: Fix the AO replacement option in the split
  kernel
  (blender/blender@ef816f9cff).
- Fix unreported: Cycles: Fix over-allocation of triangles storage for
  triangle primitive hair
  (blender/blender@be60e9b8c5).
- Fix unreported: Cycles: Fix missing type declaration in OpenCL image
  (blender/blender@4846184095).
- Fix unreported: Cycles: Fix crash when assigning KernelGlobals
  (blender/blender@ed688e4843).
- Fix unreported: Cycles: Fix access array index of -1 in SSS and volume
  split kernels
  (blender/blender@2eb906e1b4).
- Fix unreported: \[Cycles\] Fix math problems in safe_logf
  (blender/blender@c9451f1cff).
- Fix unreported: Cycles: Fix transform addressing in the denoiser code
  (blender/blender@e518ea9b5e).
- Fix unreported: Cycles: Fix occasional black pixels from denoising
  with excessive radii
  (blender/blender@58a0c27546).
- Fix unreported: Cycles: fix AO approximation for split kernel
  (blender/blender@90b9467861).
- Fix unreported: Cycles Denoising: Fix wrong order of denoising feature
  passes
  (blender/blender@b3a3459e1a).
- Fix unreported: Cycles: Fix random noise pattern seen with
  multiscatter bsdf and split kernel
  (blender/blender@29f4a8510c).
- Fix unreported: Cycles: Fix denoising passes being written when
  they're not actually generated
  (blender/blender@c73206acc5).
- Fix unreported: Cycles: Fix race condition happening in progress
  utility
  (blender/blender@794311c92b).
- Fix unreported: Cycles: Fix excessive sampling weight of glossy
  Principled BSDF components
  (blender/blender@1979176088).
- Fix unreported: Fix principled BSDF incorrectly missing subsurface
  component with base color black
  (blender/blender@52b9516e03).
- Fix unreported: Cycles: Fix comparison in principled BSDF
  (blender/blender@1f933c94a7).
- Fix unreported: Fix potential memory leak in Cycles loading of
  packed/generated images
  (blender/blender@a4cd7b7297).
- Fix unreported: Fix Cycles denoising NaNs with a 1 sample renders
  (blender/blender@ec831ee7d1).
- Fix unreported: Fix Cycles multi scatter GGX different render results
  with Clang and GCC
  (blender/blender@9e929c911e).
- \[RC2\] Fix unreported: Cycles: Fixed broken camera motion blur when
  motion was not set to center on frame
  (blender/blender@10764d31d4).
- \[Release\] Fix unreported: Fix threading conflict when doing Cycles
  background render
  (blender/blender@9997f5ca91).
- \[Release\] Fix unreported: Cycles: Fix stack overflow during
  traversal caused by floating overflow
  (blender/blender@75e392ae9f).
- \[Release\] Fix unreported: Cycles: FIx issue with -0 being considered
  a non-finite value
  (blender/blender@95d871c1a2).
- \[Release\] Fix unreported: Cycles Bake: Fix overflow when using
  hundreds of images
  (blender/blender@9ca3d4cbbd).
- \[Release\] Fix unreported: Cycles: Safer fix for infinite recursion
  (blender/blender@fbb4be061c).

### Render: Freestyle

- Fix [\#49479](http://developer.blender.org/T49479): Freestyle
  inconsistent line drawing with large geometry dimension
  (blender/blender@7f262acb92).

<!-- -->

- Fix unreported: Fix unfreed memory after cleaning render layers
  (blender/blender@18cf3e1a38).
- Fix unreported: Freestyle: Fix (unreported) wrong distance calculation
  in the Fill Range by Selection operator
  (blender/blender@cdff659036).
- Fix unreported: Freestyle: Fix (unreported) wrong distance calculation
  in the Fill Range by Selection operator
  (blender/blender@cdff659036).
- Fix unreported: Fix freestyle lineset panels being animatable
  (blender/blender@b0ee9aaa5d).

### Sequencer

- Fix [\#49996](http://developer.blender.org/T49996): VSE opengl render
  crash with grease pencil if current frame is empty
  (blender/blender@674c3bf894).
- Fix [\#49893](http://developer.blender.org/T49893): Crash in Video
  Sequence Editor with 'drop' effect
  (blender/blender@8f29503b52).
- Fix [\#51556](http://developer.blender.org/T51556): Sequencer - White
  Balance Modifier - Masking is not honored
  (blender/blender@a5c73129c5).
- Fix [\#50112](http://developer.blender.org/T50112): Sequencer crash w/
  missing proxy data
  (blender/blender@2580c3cb82).
- Fix [\#51661](http://developer.blender.org/T51661): Swaping strips
  does not refresh sequencer
  (blender/blender@a51dccc6f3).
- Fix [\#51947](http://developer.blender.org/T51947): failure setting
  sequence.use_proxy/crop/translation to False
  (blender/blender@79f27fc8d3).
- Fix [\#51898](http://developer.blender.org/T51898): missing sequence
  strip color space validation on load
  (blender/blender@00f3ab2fb2).
- \[Release\] Fix [\#52522](http://developer.blender.org/T52522): VSE
  renders with alpha transparent PNG image incorrectly
  (blender/blender@a8bd08ffdd).

<!-- -->

- Fix unreported: Fix (unreported) Sequencer Drop effect: wrong initial
  offset in second input buffer
  (blender/blender@fc4a51e3fa).
- Fix unreported: Sequencer: Fix broken interface script since 415ff74
  (blender/blender@5e82981f47).
- Fix unreported: Fix byte-to-float conversion when using scene strips
  in sequencer with identical color spaces
  (blender/blender@06ac6ded66).
- Fix unreported: Fix crash opening really old files with compositor
  (blender/blender@0e46da76b7).
- Fix unreported: Better fix for sequencer crash when text strip doesn't
  have effect data
  (blender/blender@aea4456101).

## UI / Spaces / Transform

### 3D View

- Fix [\#49408](http://developer.blender.org/T49408): OpenGL light
  widget breaks viewport shading
  (blender/blender@78c0bc52de).
- Fix [\#49804](http://developer.blender.org/T49804): Display grid
  Scale/Subdivision are sometimes disabled in View3D when they should
  not
  (blender/blender@b51874437d).
- Fix [\#49872](http://developer.blender.org/T49872): 3D cursor places
  with camera shift in ortographic mode
  (blender/blender@0a26904a75).
- Fix [\#49861](http://developer.blender.org/T49861): Interlace stereo
  is broken in 2.78
  (blender/blender@070f22c440).
- Fix [\#50564](http://developer.blender.org/T50564): 3D view panning
  with scroll wheel inconsistent with dragging
  (blender/blender@3f5b2e2682).
- Fix [\#51216](http://developer.blender.org/T51216): SSAO attenuation
  not being scale invariant
  (blender/blender@e280c70aa9).
- Fix [\#51324](http://developer.blender.org/T51324): Auto-Depth fails
  rotating out of camera
  (blender/blender@af3f7db4ec).
- Fix [\#51434](http://developer.blender.org/T51434): Module math
  operation is wrong in GLSL shading
  (blender/blender@ffc95a33b6).
- Fix [\#51354](http://developer.blender.org/T51354): Final take on
  multi-view (single view) issues
  (blender/blender@62aa925c11).
- Fix [\#51538](http://developer.blender.org/T51538): Weight-paint
  circle select w/ clipping
  (blender/blender@102394a323).
- Fix [\#51629](http://developer.blender.org/T51629): Select w/ object
  lock fails
  (blender/blender@4badf67739).
- Fix [\#51862](http://developer.blender.org/T51862): principled shader
  GLSL artifacts in ortho mode
  (blender/blender@968093ec2f).
- Fix [\#51834](http://developer.blender.org/T51834): Active Object and
  Groups color difference imperceptible
  (blender/blender@4d124418b7).
- \[Release\] Fix [\#52663](http://developer.blender.org/T52663): Remap
  used invalid local-view data
  (blender/blender@b895c7337e).

<!-- -->

- Fix unreported: fix for potential pitfall with glMatrixMode
  (blender/blender@a3d258bfb4).
- Fix unreported: Viewport SSAO: Fix normals not normalized
  (blender/blender@0507b3e4c4).
- Fix unreported: Fix possible crash in various 3D View operators
  (blender/blender@7359cc1060).
- Fix unreported: Rigid body: fix viewport not updating on properties
  change
  (blender/blender@15fa806160).
- Fix unreported: Fix stereoscopic camera volume drawing
  (blender/blender@2ad1124372).
- Fix unreported: Partial fix to Multi-View single eye issues in
  viewport
  (blender/blender@d2f1f80a6f).
- Fix unreported: Fixup for multi-view single eye viewport issues
  (blender/blender@195d0fbae3).
- Fix unreported: Fix GPencil depth checks
  (blender/blender@b5a976ec19).
- Fix unreported: Fix bad index use drawing deformed face centers
  (blender/blender@e2b1c70b48).

### Input (NDOF / 3D Mouse)

- Fix [\#48911](http://developer.blender.org/T48911): Fix
  [\#48847](http://developer.blender.org/T48847): Issues with some
  shortcuts on non-US latin keyboards, and with non-first layouts
  (blender/blender@16cb939163).
- Fix [\#49303](http://developer.blender.org/T49303): Fix
  [\#49314](http://developer.blender.org/T49314): More issues with new
  handling of X11 shortcuts
  (blender/blender@037df2aaa6).
- Fix [\#51948](http://developer.blender.org/T51948): pen pressure not
  detected with some Wacom tablets
  (blender/blender@c3c0495b30).

<!-- -->

- Fix unreported: GHOST X11 keyboard: Attempt to fix issues with
  modifier keys on some systems
  (blender/blender@87b3faf557).
- Fix unreported: Fix Windows mouse wheel scroll speed
  (blender/blender@26d7d995db).

### Outliner

- Fix [\#51680](http://developer.blender.org/T51680): 'Delete Group'
  from Group view of Outliner does not work
  (blender/blender@f783efd127).
- Fix [\#51926](http://developer.blender.org/T51926): Selecting pose
  icon under expanded group in outliner causes crash
  (blender/blender@a57a7975a1).
- \[Release\] Fix [\#52538](http://developer.blender.org/T52538):
  Outliner crash when displaying groups and using Show Active on
  editmode bone not in any groups
  (blender/blender@42760d922e).

<!-- -->

- Fix unreported: Fix outliner contextual menu allowing to delete
  indirect libraries
  (blender/blender@fa9bd04483).
- Fix unreported: Fix missing undo pushes in outliner's new datablock
  management operations
  (blender/blender@1f65ab606b).

### Text Editor

- Fix [\#50900](http://developer.blender.org/T50900): Text-Blocks
  created from "Edit Source" have zero users
  (blender/blender@15eb83c8b3).

### Transform

- Fix [\#50486](http://developer.blender.org/T50486): Don't always do
  the `ray_start_correction` in the ortho view
  (blender/blender@88b0b22914).
- Fix [\#49632](http://developer.blender.org/T49632): Grease pencil in
  "Edit Strokes" mode: Snap tool did not snap points to active object A
  simple confusion between enums: ~SNAP_NOT_ACTIVE~
  (blender/blender@997a210b08).
- Fix [\#49494](http://developer.blender.org/T49494):
  snap_align_rotation should use a local pivot to make the
  transformation
  (blender/blender@ddf99214dc).
- Fix [\#50125](http://developer.blender.org/T50125): Shortcut keys
  missing in menus for Clear Location, Rotation, and Scale
  (blender/blender@52696a0d3f).
- Fix [\#46892](http://developer.blender.org/T46892): snap to closest
  point now works with Individual Origins
  (blender/blender@21f3767809).
- Fix [\#50592](http://developer.blender.org/T50592): Scene.raycast not
  working
  (blender/blender@47caf343c0).
- Fix [\#50602](http://developer.blender.org/T50602): Avoid crash when
  executing `transform_snap_context_project_view3d_mixed` with
  `dist_px` NULL
  (blender/blender@22156d951d).
- Fix [\#50565](http://developer.blender.org/T50565): Planar constraints
  don't work properly with non-Blender key configurations
  (blender/blender@278fce1170).
- Fix [\#47690](http://developer.blender.org/T47690): Connected PET w/
  individual origins
  (blender/blender@12e681909f).
- Fix [\#50899](http://developer.blender.org/T50899): Even though the
  Shrinkwrap options hide the possibility of using a non-mesh target,
  you can still circumvent this... Causing Crash
  (blender/blender@a81ea40836).
- Fix [\#51169](http://developer.blender.org/T51169): Push/pull fails w/
  local lock axis
  (blender/blender@9737401688).
- Fix [\#51651](http://developer.blender.org/T51651): translate w/
  individual origins fails
  (blender/blender@3be073807b).
- Fix [\#51691](http://developer.blender.org/T51691): Shear cursor input
  scales w/ zoom
  (blender/blender@863f0434ec).
- Fix [\#51756](http://developer.blender.org/T51756): Fix crash when
  transforming vertices in edit mode
  (blender/blender@d583af0026).
- \[Release\] Fix [\#52490](http://developer.blender.org/T52490): NDOF
  orbit doesn't lock in ortho view
  (blender/blender@0ed5605bd5).

<!-- -->

- Fix unreported: Fix unreported bug: parameter ray_start repeated
  (blender/blender@318ee2e8c1).
- Fix unreported: Fix bug not reported: Ruler/Protractor: Snap to
  vertices and edges was not considering the depth variation
  (blender/blender@d07e2416db).
- Fix unreported: Fix (unreported) crash in new snap code
  (blender/blender@a2c469edc2).
- Fix unreported: Fix unreported bug: Ensure you have the correct array
  directory even after the `dm-\>release(dm)`
  (blender/blender@631ecbc4ca).
- Fix unreported: Fix second part
  [\#50565](http://developer.blender.org/T50565): Using planar transform
  once makes it enabled by default
  (blender/blender@499faa8b11).
- Fix unreported: Fix another part of
  [\#50565](http://developer.blender.org/T50565): Planar constraints
  were always initialized to accurate transform
  (blender/blender@87f8bb8d1d).
- Fix unreported: Fix buffer read error w/ 2 pass select queries
  (blender/blender@2462320210).
- Fix unreported: Fix unpredictable trackball rotation
  (blender/blender@9210a4faf3).
- Fix unreported: Fix [http://developer.blender.org/T51595
  \#51595](http://developer.blender.org/T51595_#51595): Snap
  to edge does not work with high zoom level
  (blender/blender@3f39719b5d).
- Fix unreported: Snap System: fix rename `ob` to `obj`
  (blender/blender@c6ddef7359).
- Fix unreported: Snap System: Fixed index of objects used to make
  `snap to volume`
  (blender/blender@8009564503).
- \[Release\] Fix unreported: Correction to last fix
  (blender/blender@7997646c2d).
- \[Release\] Fix unreported: Fix transform snap code using 'allocated'
  flags to get verts/edges/etc. arrays again from DM
  (blender/blender@9cc7e32f39).

### User Interface

- Fix [\#49383](http://developer.blender.org/T49383): Color pickers are
  available if the color is locked
  (blender/blender@09925d52f5).
- Fix [\#49997](http://developer.blender.org/T49997): don't flip texture
  users menu in texture properties
  (blender/blender@cc8132b0c8).
- Fix [\#50022](http://developer.blender.org/T50022): "Mirror" in
  Dopesheet Crashes Blender
  (blender/blender@0cd1b5ef85).
- Fix [\#50063](http://developer.blender.org/T50063): Editing driver's
  expression eliminates "Zero" number
  (blender/blender@3fb11061ba).
- Fix [\#50386](http://developer.blender.org/T50386): Crash when
  spawning pie menus
  (blender/blender@8a6c689f30).
- Fix [\#50570](http://developer.blender.org/T50570): pressing pgup or
  pgdn in any scrollable area irreversably alters scrolling speed
  (blender/blender@fb61711b1a).
- Fix [\#50497](http://developer.blender.org/T50497): prop_search not
  correctly drew in UI (D2473)
  (blender/blender@feb588060a).
- Fix [\#50629](http://developer.blender.org/T50629): Add remove doubles
  to the cleanup menu
  (blender/blender@bb1367cdaf).
- Fix [\#51068](http://developer.blender.org/T51068): Place props in
  their own row
  (blender/blender@93426cb295).
- Fix [\#51248](http://developer.blender.org/T51248): user preferences
  window size not adapted to DPI
  (blender/blender@6c26911c3d).
- Fix [\#50775](http://developer.blender.org/T50775): Missing
  parenthesis on fluid bake button
  (blender/blender@76c97aaff9).
- Fix [\#51737](http://developer.blender.org/T51737): Material
  properties error
  (blender/blender@5ccaef6d67).
- Fix [\#51845](http://developer.blender.org/T51845): UI Scale cause
  double width vertical borders
  (blender/blender@bddb4de47c).
- Fix [\#51772](http://developer.blender.org/T51772): double undo entry
  for color picker editing
  (blender/blender@eb1532a860).
- Fix [\#49034](http://developer.blender.org/T49034): multi-drag crashes
  when UI forces exit
  (blender/blender@d415a1cbd6).
- Fix [\#49498](http://developer.blender.org/T49498): continuous grab
  issues on macOS, particularly with gaming mouses
  (blender/blender@8b2785bda5).
- Fix [\#51776](http://developer.blender.org/T51776): Make sure button
  icons are updated on Ctrl-ScrollWheel
  (blender/blender@f1d6bad4b6).
- Fix [\#52208](http://developer.blender.org/T52208): Using
  UI_BUT_REDALERT flag for UI_BTYPE_KEY_EVENT buttons crashes Blender
  (blender/blender@304e5541cb).
- \[RC2\] Fix [\#52250](http://developer.blender.org/T52250): Glitch in
  UI in the addon panel regression
  (blender/blender@ade9fc9245).
- \[RC2\] Fix [\#52263](http://developer.blender.org/T52263): Crash When
  Splitting and Merging Areas with Header Text Set
  (blender/blender@fb73cee1ec).
- \[Release\] Fix [\#52466](http://developer.blender.org/T52466):
  Silence search for button_context menu type
  (blender/blender@8cb217069e).
- \[Release\] Fix [\#51400](http://developer.blender.org/T51400):
  Pasting hex code fails
  (blender/blender@8193e50fd3).

<!-- -->

- Fix unreported: Fix menu drawing printing 'unknown operator' warning
  when building without WITH_BULLET
  (blender/blender@c126a5179f).
- Fix unreported: Fix vertical scrollbar adding to region width in graph
  editor
  (blender/blender@df3394a386).
- Fix unreported: Fix jumping view when expanding graph editor channel
  over view bounds
  (blender/blender@4b39069908).
- Fix unreported: UI: Fix crash using drag-toggle over window bounds
  with button callback
  (blender/blender@c5326958a5).
- Fix unreported: Fix crash in space context cycling when leaving window
  bounds
  (blender/blender@2476faebd7).
- Fix unreported: GPencil: Fix interpolate stroke keymap conflict with
  sculpt
  (blender/blender@b859557115).
- Fix unreported: More fixes for keyframe theme options
  (blender/blender@2a53e0975c).
- Fix unreported: Fix unlikely uninitialized pointer usage
  (blender/blender@0b749d57ee).
- Fix unreported: Fix Make Vertex Parent operator missing from
  vertex/curve/lattice menus
  (blender/blender@4151f12713).
- Fix unreported: Fix menu inconsistencies
  (blender/blender@cb117f283b).
- Fix unreported: Fix prefs UI when built w/o Cycles
  (blender/blender@403f00e558).
- Fix unreported: Fix expanding enum property in sub-layout of pie menus
  (blender/blender@d5708fdad6).
- Fix unreported: Fix drawing enum property with icon only flag
  (blender/blender@524ab96245).
- Fix unreported: UI Layout: fix some cases mixing fixed and expandable
  sizes
  (blender/blender@da026249ab).
- Fix unreported: Fix blurry icons
  (blender/blender@15b253c082).
- Fix unreported: Fix Drawing nested box layouts (D2508)
  (blender/blender@3622074bf7).
- Fix unreported: Fix rows with fixed last item (D2524)
  (blender/blender@94ca09e01c).
- Fix unreported: Fix text and icon positioning issues
  (blender/blender@32c5f3d772).
- Fix unreported: Fix width calculation for split layouts
  (blender/blender@f1c764fd8f).
- Fix unreported: Fix icon alignment for pie buttons
  (blender/blender@76015f98ae).
- Fix unreported: Fix for splash not opening centered
  (blender/blender@253281f9d6).
- Fix unreported: Fix various i18n ambiguous issues reported in
  [\#43295](http://developer.blender.org/T43295)
  (blender/blender@a7f16c17c2).
- Fix unreported: Fix: Button's label can be NULL
  (blender/blender@001fce167a).
- Fix unreported: Fix: Use "round" instead of "floor" in snapping UI to
  pixels
  (blender/blender@fa63515c37).
- Fix unreported: Fix: Ignore min flag for rows that require all
  available width
  (blender/blender@4bdb2d4885).
- Fix unreported: Fix padding and align calculation for box layouts
  (blender/blender@505b3b7328).
- Fix unreported: Fix columns with fixed width
  (blender/blender@5ce120b865).
- Fix unreported: Fix: Icon offset for pie buttons
  (blender/blender@59bb4ca1b0).
- Fix unreported: Fix: width of UILayout.prop_enum() buttons
  (blender/blender@31bdb31ecf).
- Fix unreported: Fix UI: double separator in Movie Clip Editor's view
  menu
  (blender/blender@8d78df315c).
- Fix unreported: Fix 'API defined' ID properties still having 'remove'
  button in UI
  (blender/blender@42c8d93c5f).
- Fix unreported: UI: Fix some small ui inconsistencies
  (blender/blender@f0bbb67e8a).
- Fix unreported: Fix: Icon alignment for scaled pie buttons with no
  text
  (blender/blender@9e08019b74).
- Fix unreported: Fix node UI not using translation context correctly
  (blender/blender@b5696f2799).
- Fix unreported: Fix bad loss of precision when manually editing values
  in numbuttons
  (blender/blender@ecb5b55d7f).
- Fix unreported: Fix: use click style if a pie was spawned by release
  or click event
  (blender/blender@3e8b2288f5).
- Fix unreported: Fix Label colors in popups
  (blender/blender@ec22809025).
- Fix unreported: Fix button text overlapping with shortcut text in
  popups
  (blender/blender@920bff5224).
- Fix unreported: Fix potential 'divide-by-zero' in our UI fitting code
  (blender/blender@38eabcb858).
- \[RC2\] Fix unreported: Fix width estimation for empty layouts in pie
  menus
  (blender/blender@205202361c).
- \[RC2\] Fix unreported: Fix fixed width box layouts
  (blender/blender@909320e3ff).
- \[RC2\] Fix unreported: Fix width estimation for buttons with short
  labels in pie menus
  (blender/blender@686b8e8fed).
- \[Release\] Fix unreported: UI: fix memory leak when copy-to-selected
  failed
  (blender/blender@9da098536d).

## Game Engine

- Fix [\#50098](http://developer.blender.org/T50098): BGE: Crash when
  useding ImageMirror
  (blender/blender@741c4082d8).

<!-- -->

- Fix unreported: BGE: Fix silly typo that invalidates negative scaling
  camera feature
  (blender/blender@89b1805df6).

## System / Misc

### Audio

- Fix [\#49657](http://developer.blender.org/T49657): Audio backend
  "Jack" should be named "JACK"
  (blender/blender@3055ae5092).
- Fix [\#49657](http://developer.blender.org/T49657): Audio backend
  "Jack" should be named "JACK"
  (blender/blender@132478d4b8).
- Fix [\#50065](http://developer.blender.org/T50065): Audaspace: some
  values of the lower limit of Factory.limit causes the factory not to
  play
  (blender/blender@3340acd46b).
- Fix [\#50240](http://developer.blender.org/T50240): Rendering crashes
  when synced to JACK Transport
  (blender/blender@d874b40a55).
- Fix [\#50843](http://developer.blender.org/T50843): Pitched Audio
  renders incorrectly in VSE
  (blender/blender@f75b52eca1).
- \[Release\] Fix [\#52472](http://developer.blender.org/T52472): VSE
  Audio Volume not set immediately
  (blender/blender@022b9676b0).

<!-- -->

- Fix unreported: Fix: setting an audio callback before audio device
  initialization
  (blender/blender@e9689e1a20).
- Fix unreported: Fix: Audio plays back incorrectly after rendering to a
  video file
  (blender/blender@e713009e9b).
- Fix unreported: Fix potential memory leak in Sequencer sound strip
  creation code
  (blender/blender@b488988ab1).

### Collada

- Fix [\#50004](http://developer.blender.org/T50004): Removed check for
  empty mesh and adjusted the vertex import function to accept meshes
  without vertices as well
  (blender/blender@447fc7c4ce).
- Fix [\#50118](http://developer.blender.org/T50118): Added missing
  assignment of Bone Roll
  (blender/blender@d464a7c441).
- Fix [\#50923](http://developer.blender.org/T50923): Inconsistent
  default values and wrong order of parameters in api call
  (blender/blender@b759d3c9c5).
- Fix [\#52065](http://developer.blender.org/T52065): Joint ID was
  generated wrong for bone animation exports
  (blender/blender@9feeb14e91).

<!-- -->

- Fix unreported: fix D2489: Collada exporter broke edit data when
  exporting Armature while in Armature edit mode
  (blender/blender@c64c901535).
- Fix unreported: fix D2489: Collada exporter broke edit data when
  exporting Armature while in Armature edit mode
  (blender/blender@ba116c8e9c).
- Fix unreported: fix D2552: Collada - use unique id for bones with same
  name but in different armatures. Co-authored-by: Gaia
  \<gaia.clary@machiniamtrix.org\>
  (blender/blender@da6cd77628).
- Fix unreported: fix: collada - Connected bones get their tails set to
  wrong location when fix leaf nodes option is enabled
  (blender/blender@ec3989441f).
- Fix unreported: fix: [\#50412](http://developer.blender.org/T50412) -
  collada: Replaced precision local limit function by blender's own
  implementation
  (blender/blender@3bf0026bec).
- Fix unreported: fix: collada - do proper conversion from int to bool
  (as with other nearby parameters)
  (blender/blender@f65d6ea954).
- Fix unreported: Fix collada importer doing own handling of
  usercount/freeing
  (blender/blender@7853ebc204).
- Fix unreported: Collada: Fix: Geometry exporter did not create all
  polylist when meshes are only partially textured
  (blender/blender@bdacb60a92).
- Fix unreported: fix: adjusted collada declaration after changes in
  collada module. @campbell Barton: Why is this declaration needed at
  all in stubs.c? Further up the file collada.h is imported and that
  already decalres the function and results in a duplicate declaration
  (blender/blender@d17786b107).
- Fix unreported: fix: collada: removed unnecessary extra qualification
  (blender/blender@1340c7bcdc).
- Fix unreported: fix: Collada fprintf needs std::string be converted to
  char \*
  (blender/blender@fcb8761966).
- Fix unreported: fix: [\#51622](http://developer.blender.org/T51622)
  The exporter now exports meshes as <Triangles> when all contained
  polygons are tris
  (blender/blender@c9b95c28f6).

### File I/O

- Fix [\#49352](http://developer.blender.org/T49352): Blender's file
  browser do not display previews
  (blender/blender@51e8c167f4).
- Fix [\#49369](http://developer.blender.org/T49369): Blender
  crashes/closes down application at alembic export of any object
  (blender/blender@04bfea0d67).
- Fix [\#49878](http://developer.blender.org/T49878): Alembic crash with
  long object name
  (blender/blender@d3b0977a35).
- Fix [\#49918](http://developer.blender.org/T49918): Make duplicates
  real crash on clicking operator toggles
  (blender/blender@4e5d251ccb).
- Fix [\#50013](http://developer.blender.org/T50013): Blender 2.78a
  Link/Append Crash
  (blender/blender@1b1d6ce131).
- Fix [\#49813](http://developer.blender.org/T49813): crash after
  changing Alembic cache topology
  (blender/blender@66a3671904).
- Fix [\#50334](http://developer.blender.org/T50334): Also select
  indirectly imported objects when linking/appending
  (blender/blender@934b3f3682).
- Fix [\#50287](http://developer.blender.org/T50287): Blender crashes
  when open a blend that contains an alembic file
  (blender/blender@b91edd61d0).
- Fix [\#49249](http://developer.blender.org/T49249): Alembic export
  with multiple hair systems crash blender
  (blender/blender@8cda364d6f).
- Fix [\#50757](http://developer.blender.org/T50757): Alembic, assign
  imported materials to the object data instead of to the object itself
  (blender/blender@caaf5f0a09).
- Fix [\#50227](http://developer.blender.org/T50227): Alembic uv
  export/load issue
  (blender/blender@699a3e2498).
- Fix [\#51262](http://developer.blender.org/T51262): Blender CRASH with
  alembic file
  (blender/blender@3128600a8a).
- Fix [\#51292](http://developer.blender.org/T51292): Alembic import,
  show notification when trying to load HDF5
  (blender/blender@9dadd5ff93).
- Fix [\#51280](http://developer.blender.org/T51280): Alembic: Crash
  when removing cache modifier
  (blender/blender@ff1f115706).
- Fix [\#51432](http://developer.blender.org/T51432): Find Files case
  sensitive on win32
  (blender/blender@1cfc48192c).
- Fix [\#51336](http://developer.blender.org/T51336): Crash on broken
  file opening
  (blender/blender@baf788d7cd).
- Fix [\#51319](http://developer.blender.org/T51319): Alembic export
  crash w/simple child particles if Display value \< 100%
  (blender/blender@8d26f2c222).
- Fix [\#51534](http://developer.blender.org/T51534): Alembic: added
  support for face-varying vertex colours
  (blender/blender@7b25ffb618).
- Fix [\#51586](http://developer.blender.org/T51586): Regression:
  Alembic containing animated curves / hair no longer working
  (blender/blender@ad27e97ee7).
- Fix [\#51820](http://developer.blender.org/T51820): Alembic: for
  sequence files not loading properly
  (blender/blender@0900914d96).
- Fix [\#51015](http://developer.blender.org/T51015): Pack all into
  blend automatically leaves checkbox enabled
  (blender/blender@bdd814d87d).
- Fix [\#51052](http://developer.blender.org/T51052): CacheFile Open
  crashes from Python
  (blender/blender@2c10e8a3cf).
- Fix [\#52022](http://developer.blender.org/T52022): Alembic Inherits
  transform not taken into account
  (blender/blender@32edfd53d9).
- Fix [\#52109](http://developer.blender.org/T52109): Folder search
  won't work when selecting animation output folder
  (blender/blender@3cfb248bb6).
- \[RC2\] Fix [\#52240](http://developer.blender.org/T52240): Alembic
  Not Transferring Materials Per Frame
  (blender/blender@f21020f45f).
- \[Release\] Fix [\#52481](http://developer.blender.org/T52481): After
  making all local, local proxies of linked data get broken after file
  save and reload
  (blender/blender@fc26280bcb).
- \[Release\] Fix [\#52579](http://developer.blender.org/T52579):
  Alembic: crash when replacing slightly different alembic files
  (blender/blender@4b90830cac).

<!-- -->

- Fix unreported: CacheFile: fix missing depsgraph update
  (blender/blender@65c481e145).
- Fix unreported: Alembic export: fix frame range values being reset at
  every update, draw call
  (blender/blender@0c13792437).
- Fix unreported: Fix forward-compat Nodes write code being executed
  also for undo steps writing
  (blender/blender@5a6534a5bb).
- Fix unreported: Fix crash when opening a Blender file containing
  Alembic data
  (blender/blender@62a2ed97ba).
- Fix unreported: Fix (unreported) linked datablocks going through
  do_versions several times
  (blender/blender@bd42987399).
- Fix unreported: Fix (unreported) crash when file browser attempts to
  show preview of some defective font
  (blender/blender@f14e1da5aa).
- Fix unreported: Alembic/CacheFile: fix crash de-referencing NULL
  pointer
  (blender/blender@4580ace4c1).
- Fix unreported: Fix (unreported) Object previews being written even
  for skipped objects
  (blender/blender@f7eaaf35b4).
- Fix unreported: Alembic: fixed mistake in bounding box computation
  (blender/blender@b929eef8c5).
- Fix unreported: Alembic: fix naming of imported transforms
  (blender/blender@54102ab36e).
- Fix unreported: Fix lib_link_cachefile
  (blender/blender@e1909958d9).
- Fix unreported: Alembic: fixed importer
  (blender/blender@02d6df80aa).
- Fix unreported: Alembic import: fixed bug where local matrix from
  Alembic was used as object matrix
  (blender/blender@d1696622b7).
- Fix unreported: Alembic import: fixed crash on more complex model
  (blender/blender@bc55c19807).
- Fix unreported: Alembic export: fixed exporting as "flat"
  (blender/blender@642728b339).
- Fix unreported: Alembic import: fixed off-by-one error in start/end
  frame
  (blender/blender@0706b908db).
- Fix unreported: Alembic export: fixed flattened dupligroup import
  (blender/blender@4d117f2fd2).
- Fix unreported: Alembic import: fixed dupligroup export when the
  dupli-empty has a parent
  (blender/blender@5fa4f397c2).
- Fix unreported: Alembic export: fixed curve type and order
  (blender/blender@d24578b676).
- Fix unreported: Alembic: fixed refcount issue when duplicating
  imported objects
  (blender/blender@20621d46d1).
- Fix unreported: Proper fix for crash loading old files with compositor
  (blender/blender@3de9db9650).
- Fix unreported: Alembic import: fixed bug interpolating between frames
  (blender/blender@24a0b332e2).
- Fix unreported: Fix (unreported) seldom crash when using previews in
  filebrowser
  (blender/blender@73adf3e27d).
- \[RC2\] Fix unreported: Alembic import: fix crash when face color
  index is out of bounds
  (blender/blender@2307ea88b5).

### Other

- Fix [\#49899](http://developer.blender.org/T49899): Add
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW to classes that use eigen's data types
  , to force aligned on 16 byte boundaries
  (blender/blender@0de157a320).
- Fix [\#50305](http://developer.blender.org/T50305): Blender truncates
  a long multibyte character object's name to an invalid utf-8 string
  (blender/blender@752a783fa4).
- Fix [\#51024](http://developer.blender.org/T51024): Switch
  install_deps to set OSL_ROOT_DIR instead of CYCLES_OSL
  (blender/blender@797b1d5053).
- Fix [\#51624](http://developer.blender.org/T51624): Scene Full Copy
  ignores shader node links
  (blender/blender@be31582e3d).
- Fix [\#51889](http://developer.blender.org/T51889): broken UI after
  File \> New without a userpref.blend
  (blender/blender@46c7d45f77).
- Fix [\#51998](http://developer.blender.org/T51998): Anim player uses
  100% CPU
  (blender/blender@910ab14145).
- \[Release\] Fix [\#52396](http://developer.blender.org/T52396): Crash
  loading template w/o config dir
  (blender/blender@76e8dcafa3).

<!-- -->

- Fix unreported: API doc update script: Fix generated zipfile name, was
  broken in 'release' case
  (blender/blender@56c798ee62).
- Fix unreported: Fix (unreported) some RNA func definitions setting
  flags of other func parameters!
  (blender/blender@7415b9ffa3).
- Fix unreported: Fix (unreported) `--threads` option no more
  respected by main task scheduler
  (blender/blender@8db2f72997).
- Fix unreported: Fix (unreported) fully broken 'sanitize utf-8' helper
  (blender/blender@adadfaad88).
- Fix unreported: Fix/cleanup stupid check on array of char being
  non-NULL pointer
  (blender/blender@a97ec403c2).
- Fix unreported: Fix 'public' global 'g_atexit' var in Blender
  (blender/blender@ac8348d033).
- Fix unreported: Fix some more minor issue with updated py doc
  generation
  (blender/blender@29859d0d5e).
- Fix unreported: Fix bug on Blender version string
  (blender/blender@81dc8dd42a).
- Fix unreported: Install deps: Fix compilation error of Alembic
  (blender/blender@358def15a3).
- Fix unreported: Fix: Object.raycast: error to free treedata
  (blender/blender@34ea8058b9).
- Fix unreported: Docs: Fix <file:line> links in generated API docs
  (blender/blender@b94a433ca3).
- \[RC2\] Fix unreported: Fix broken API doc generation: Partially
  revertblender/blender@a372638a76e0
  (blender/blender@8ae6e35923).

### Python

- Fix [\#49829](http://developer.blender.org/T49829): Removal of custom
  icon previews during add-on unregister crashes Blender
  (blender/blender@5f0933f07a).
- Fix [\#49856](http://developer.blender.org/T49856): Blender 2.78
  crashes after loading data from a blendfile
  (blender/blender@1ee43c5aef).
- Fix [\#50007](http://developer.blender.org/T50007): blender offline
  python documentation in zipped HTML files, not shown correctly
  (blender/blender@7e8bf9dbd6).
- Fix [\#50052](http://developer.blender.org/T50052):
  bpy.utils.unregister_module doesn't unregister classes of submodules
  in reload scenario
  (blender/blender@8f0dc3cef6).
- Fix [\#50029](http://developer.blender.org/T50029):
  BVHTree.FromPolygons memory leak
  (blender/blender@3b467b35a8).
- Fix [\#50926](http://developer.blender.org/T50926): python crashes
  with path containing utf8 characters
  (blender/blender@43f7d5643f).
- Fix [\#51287](http://developer.blender.org/T51287): Matrix.lerp fails
  w/ shear
  (blender/blender@98df7d778f).
- Fix [\#51810](http://developer.blender.org/T51810): Add minimal
  example of usage of translation API for non-official addons
  (blender/blender@fc4154f857).
- Fix [\#52195](http://developer.blender.org/T52195): Sculpt from Python
  fails
  (blender/blender@f3782c0a9e).
- \[RC2\] Fix [\#46329](http://developer.blender.org/T46329):
  scene_update_{pre,post} doc needs clarification
  (blender/blender@6bc962d7bc).
- \[Release\] Fix [\#52515](http://developer.blender.org/T52515): Crash
  on BMesh.to_mesh()
  (blender/blender@5fd4eca8c0).

<!-- -->

- Fix unreported: Fix objects added via py being on the wrong layer when
  viewport is decoupled from scene
  (blender/blender@17603b9f01).
- Fix unreported: API: Fix Links
  (blender/blender@cf9a6b416c).
- Fix unreported: Fix custom props not being handled correctly by
  manual/pyref UI menu entries
  (blender/blender@e17b92f535).
- Fix unreported: API: Fix double slashes in URLs
  (blender/blender@911544c70c).
- Fix unreported: API: Fix redirect in bgl page
  (blender/blender@42e4b0955c).
- Fix unreported: API: Fix rst syntax
  (blender/blender@13174df534).
- Fix unreported: Fix missing uniform type for python GPU uniform export
  (blender/blender@d4e0557cf1).
- Fix unreported: Fix incorrect spot lamp blend in python GPU uniform
  export
  (blender/blender@7f10a889e3).
- Fix unreported: Fix memory leak re-registering operators
  (blender/blender@d808557d15).
- Fix unreported: Fix 'bl_app_override' wrapping multiple times
  (blender/blender@4f69dca547).
- Fix unreported: Fix PyAPI crash assigning/deleting id-properties
  (blender/blender@5cdd94a58e).
- Fix unreported: Property path generation fixes
  (blender/blender@09eac0159d).
- \[RC2\] Fix unreported: PyAPI: Fix memory leak w/ empty, allocated
  enums
  (blender/blender@9bc5549222).
- \[Release\] Fix unreported: Fix bpy library load: invalid function
  signature
  (blender/blender@fd0fbf2564).
- \[Release\] Fix unreported: Fix error in PointerProperty argument list
  (blender/blender@6692af3997).
- \[Release\] Fix unreported: PyAPI: Fix mathutils freeze allowing owned
  data
  (blender/blender@1a7dda046b).

### System

- Fix [\#49385](http://developer.blender.org/T49385): Copy buffer is not
  from pose mode. Report Error
  (blender/blender@25ee094226).
- Fix [\#49903](http://developer.blender.org/T49903): Blender crashes
  -\> Append Group incl. Object using boolean modifier
  (blender/blender@4a68ff150f).
- Fix [\#49905](http://developer.blender.org/T49905): Segfault when
  copying object data of linked object
  (blender/blender@f0ac661aa8).
- Fix [\#49991](http://developer.blender.org/T49991): reloading
  librairies doesn't update node groups
  (blender/blender@369872a2c5).
- Fix [\#50034](http://developer.blender.org/T50034): Blender changes
  processor affinity unauthorized
  (blender/blender@751573ce6f).
- Fix [\#46795](http://developer.blender.org/T46795): Reset
  GWLP_USERDATA to NULL at window destruction so any future events will
  not try to reference this deleted class
  (blender/blender@05b181fbc5).
- Fix [\#50305](http://developer.blender.org/T50305): When adding new ID
  with same name as existing, Blender could generate invalid utf-8 ID
  name
  (blender/blender@7924c84c44).
- Fix [\#50385](http://developer.blender.org/T50385): Deadlock in
  BKE_libblock_remap_locked
  (blender/blender@9c756162ae).
- Fix [\#50676](http://developer.blender.org/T50676): Crash on closing
  while frameserver rendering
  (blender/blender@9062c086b4).
- Fix [\#49655](http://developer.blender.org/T49655): Reloading library
  breaks proxies
  (blender/blender@df88d54284).
- Fix [\#50886](http://developer.blender.org/T50886): Blender crashes on
  render
  (blender/blender@a095611eb8).
- Fix [\#50788](http://developer.blender.org/T50788): blender startup
  crash on macOS with some types of volumes available
  (blender/blender@3f94836922).
- Fix [\#51150](http://developer.blender.org/T51150): user_remap on
  itself changes users to 0
  (blender/blender@4d0d1b5936).
- Fix [\#51243](http://developer.blender.org/T51243): Delete Globally
  won't work with Datablock ID Properties
  (blender/blender@1873ea337c).
- Fix [\#50856](http://developer.blender.org/T50856): crash when
  minimizing window on AMD / Windows
  (blender/blender@ce531ed1a1).
- Fix [\#51902](http://developer.blender.org/T51902): Severe problem
  with relocating linked libraries when using proxies
  (blender/blender@d4ca2ec9d5).
- Fix [\#51889](http://developer.blender.org/T51889): new file or load
  factory settings results in broken UI
  (blender/blender@7dc3ad2287).
- Fix [\#51959](http://developer.blender.org/T51959): Windows + Intel
  GPU offset between UI drawing and mouse
  (blender/blender@0584c5ba8e).
- Fix [\#52148](http://developer.blender.org/T52148): Point Density
  Texture ID User decrement error related to the Object field
  (blender/blender@05f377805b).
- \[RC2\] Fix [\#52260](http://developer.blender.org/T52260): Blender
  2.79 Objects made duplicates real still refer armature proxy
  (blender/blender@fa34f864a2).
- \[RC2\] Fix [\#52315](http://developer.blender.org/T52315): Crash on
  duplicating Scene without world
  (blender/blender@0146ab2fd5).
- \[RC2\] Fix [\#52278](http://developer.blender.org/T52278): 'Default'
  application template fails
  (blender/blender@a52ec5308f).

<!-- -->

- Fix unreported: Fix some assert when making local (due to infamous
  PITA ShapeKey ID)
  (blender/blender@18be39ff17).
- Fix unreported: Fix `BKE_library_make_local()` trying to also make
  local proxified objects
  (blender/blender@e316636fa8).
- Fix unreported: Fix two very bad issues in new ID.make_local RNA
  function
  (blender/blender@b97c567c1d).
- Fix unreported: Fix (IRC reported) bad handling of Text data-block
  user count
  (blender/blender@a9163f7d22).
- Fix unreported: Fix missing user when opening text from ID UI widget
  (blender/blender@646aa40cf7).
- Fix unreported: \[Cycles/MSVC/Testing\] Fix broken test code
  (blender/blender@64f5afdb89).
- Fix unreported: Fix missing non-ID nodetrees in ID relationships built
  from library_query.c
  (blender/blender@fbd28d375a).
- Fix unreported: Fix ugly mistake in BLI_task - freeing while some
  tasks are still being processed
  (blender/blender@18c2a44333).
- Fix unreported: Fix GHOST crash on X11 with recent DPI changes on some
  systems
  (blender/blender@393efccb19).
- Fix unreported: Fix crash closing window in background mode
  (blender/blender@a7ca991841).
- Fix unreported: Fix missing protection of `RNA_pointer_as_string()`
  against NULL pointers
  (blender/blender@9170e49250).
- Fix unreported: Fix (unreported) missing handling of GPencil Layer's
  parent Object pointer in BKE_library_query
  (blender/blender@1c28e12414).
- Fix unreported: Fix use after free of new render layer ID properites
  after copying scene
  (blender/blender@ffa31a8421).
- Fix unreported: Fix missing usercount update of poselib when copying
  Object
  (blender/blender@b180900e52).
- Fix unreported: Fix bad handling of 'extra' user for groups at their
  creation
  (blender/blender@8b0f968a31).
- Fix unreported: Fix crash when making local object+obdata with linked
  armature
  (blender/blender@f097e73a2a).
- Fix unreported: Fix three obvious mistakes in brush/mask/cachefile ID
  copying
  (blender/blender@31437b0d4d).
- Fix unreported: Fix potentially dnagerous code in doversionning of
  brush
  (blender/blender@25c0666b90).
- Fix unreported: Fix dangerous code when deleting Scene
  (blender/blender@ee5ed2ae26).
- Fix unreported: Guarded allocator: Fix type in macro definition
  (blender/blender@8bf108dd48).
- Fix unreported: Fix ED_OT_undo_redo operator
  (blender/blender@5b2b5a4258).
- Fix unreported: Fix (unreported) Scene copying doing very stupid
  things with World and LineStyle usercounts
  (blender/blender@806bc4b433).
- Fix unreported: Fix (unreported) Scene's copying toolsettings' clone
  and canvas, and particles' scene and object pointers
  (blender/blender@665288ccd7).
- Fix unreported: Fix (unreported) broken uvsculpt in Scene's
  toolsettings' copying
  (blender/blender@8677c76f13).
- Fix unreported: Fix factory setup using user scripts path still
  (blender/blender@4003409430).
- \[RC2\] Fix unreported: Fix OSX duplicate path in Python's sys.path
  (blender/blender@6e7d962118).

<hr/>

## RC2

For RC2, 25 bugs were fixed:

- Fix [\#52327](http://developer.blender.org/T52327): Entering/Exiting
  NLA Tweakmode disables Scene -\> Only Keyframes from Selected Channels
  (blender/blender@3689be736b).
- Fix [\#52401](http://developer.blender.org/T52401): "Export Keying
  Set" operator generated incorrect ID's for shapekeys
  (blender/blender@7b397cdfc8).
- Fix [\#52255](http://developer.blender.org/T52255): New Depsgraph -
  Constraint and Drivers not working together when the driver references
  itself
  (blender/blender@b1d998ec5d).
- Fix [\#52329](http://developer.blender.org/T52329): Boolean with
  aligned shapes failed
  (blender/blender@0cb38b2875).
- Fix [\#52324](http://developer.blender.org/T52324): Metaball
  disappears when deleting first metaball object
  (blender/blender@ff47118c73).
- Fix [\#51701](http://developer.blender.org/T51701): Alembic cache
  screws up mesh
  (blender/blender@8138082ab8).
- Fix [\#52344](http://developer.blender.org/T52344): Softbody on Text
  (blender/blender@efa840f99f).
- Fix [\#52344](http://developer.blender.org/T52344): Softbody on Text
  (blender/blender@9a239eea68).
- Fix [\#52334](http://developer.blender.org/T52334): images with
  non-color data should not change color space on save
  (blender/blender@35f5d80f3a).
- Fix [\#52280](http://developer.blender.org/T52280): The Image node in
  Compositing can't read Z buffer of openEXR in 2.79
  (blender/blender@e54df78c82).
- Fix unreported: Cycles: Fixed broken camera motion blur when motion
  was not set to center on frame
  (blender/blender@10764d31d4).
- Fix [\#52250](http://developer.blender.org/T52250): Glitch in UI in
  the addon panel regression
  (blender/blender@ade9fc9245).
- Fix [\#52263](http://developer.blender.org/T52263): Crash When
  Splitting and Merging Areas with Header Text Set
  (blender/blender@fb73cee1ec).
- Fix unreported: Fix width estimation for empty layouts in pie menus
  (blender/blender@205202361c).
- Fix unreported: Fix fixed width box layouts
  (blender/blender@909320e3ff).
- Fix unreported: Fix width estimation for buttons with short labels in
  pie menus
  (blender/blender@686b8e8fed).
- Fix [\#52240](http://developer.blender.org/T52240): Alembic Not
  Transferring Materials Per Frame
  (blender/blender@f21020f45f).
- Fix unreported: Alembic import: fix crash when face color index is out
  of bounds
  (blender/blender@2307ea88b5).
- Fix unreported: Fix broken API doc generation: Partially
  revertblender/blender@a372638a76e0
  (blender/blender@8ae6e35923).
- Fix [\#46329](http://developer.blender.org/T46329):
  scene_update_{pre,post} doc needs clarification
  (blender/blender@6bc962d7bc).
- Fix unreported: PyAPI: Fix memory leak w/ empty, allocated enums
  (blender/blender@9bc5549222).
- Fix [\#52260](http://developer.blender.org/T52260): Blender 2.79
  Objects made duplicates real still refer armature proxy
  (blender/blender@fa34f864a2).
- Fix [\#52315](http://developer.blender.org/T52315): Crash on
  duplicating Scene without world
  (blender/blender@0146ab2fd5).
- Fix [\#52278](http://developer.blender.org/T52278): 'Default'
  application template fails
  (blender/blender@a52ec5308f).
- Fix unreported: Fix OSX duplicate path in Python's sys.path
  (blender/blender@6e7d962118).

## Release

For Release, 50 bugs were fixed:

- Fix unreported: Fix bpy library load: invalid function signature
  (blender/blender@fd0fbf2564).
- Fix [\#52434](http://developer.blender.org/T52434): Restore mesh
  center of mass calculation
  (blender/blender@ba600ff7fa).
- Fix unreported: Fix error in PointerProperty argument list
  (blender/blender@6692af3997).
- Fix [\#52439](http://developer.blender.org/T52439): Crash after
  adjusting lenght of hair particles
  (blender/blender@d2f20aed04).
- Fix [\#52473](http://developer.blender.org/T52473): blender internal
  Fresnel and Layer Weight only work with linked normal
  (blender/blender@dd84324485).
- Fix [\#52454](http://developer.blender.org/T52454): Crash in
  DEG_graph_on_visible_update when activating scene layer
  (blender/blender@63e21e7218).
- Fix unreported: Fix threading conflict when doing Cycles background
  render
  (blender/blender@9997f5ca91).
- Fix [\#52466](http://developer.blender.org/T52466): Silence search for
  button_context menu type
  (blender/blender@8cb217069e).
- Fix [\#52218](http://developer.blender.org/T52218): Missing update
  when reconnecting node
  (blender/blender@f2aa9bec9d).
- Fix [\#51805](http://developer.blender.org/T51805): Overlapping
  volumes renders incorrect on AMD GPU
  (blender/blender@6825439b36).
- Fix unreported: Cycles: Fix stack overflow during traversal caused by
  floating overflow
  (blender/blender@75e392ae9f).
- Fix [\#52481](http://developer.blender.org/T52481): After making all
  local, local proxies of linked data get broken after file save and
  reload
  (blender/blender@fc26280bcb).
- Fix [\#52538](http://developer.blender.org/T52538): Outliner crash
  when displaying groups and using Show Active on editmode bone not in
  any groups
  (blender/blender@42760d922e).
- Fix [\#52478](http://developer.blender.org/T52478): Error report
  "Shrinkwrap: out of memory" on invisible target
  (blender/blender@a5213924a8).
- Fix [\#52498](http://developer.blender.org/T52498): Deleting force
  field doesn't remove "Surface" from modifier stack
  (blender/blender@82a6889d83).
- Fix [\#52588](http://developer.blender.org/T52588): Shape key value
  driver variables of duplicated object sets refer to old objects
  (blender/blender@e9ca9dd5d7).
- Fix [\#52472](http://developer.blender.org/T52472): VSE Audio Volume
  not set immediately
  (blender/blender@022b9676b0).
- Fix [\#52227](http://developer.blender.org/T52227): Time Slide tool
  doesn't take NLA mapping into account
  (blender/blender@73cdf00ea8).
- Fix [\#52396](http://developer.blender.org/T52396): Crash loading
  template w/o config dir
  (blender/blender@76e8dcafa3).
- Fix [\#52490](http://developer.blender.org/T52490): NDOF orbit doesn't
  lock in ortho view
  (blender/blender@0ed5605bd5).
- Fix unreported: Correction to last fix
  (blender/blender@7997646c2d).
- Fix unreported: Fix minor Mesh -\> BMesh conversion issues
  (blender/blender@6fec06926e).
- Fix [\#52515](http://developer.blender.org/T52515): Crash on
  BMesh.to_mesh()
  (blender/blender@5fd4eca8c0).
- Fix [\#51400](http://developer.blender.org/T51400): Pasting hex code
  fails
  (blender/blender@8193e50fd3).
- Fix [\#52483](http://developer.blender.org/T52483): Fill is incorrect
  for interpolated strokes
  (blender/blender@27c42e05c4).
- Fix unreported: Fix: GPencil Sequence Interpolation for
  thickness/strength was inverted
  (blender/blender@ed0429e7e6).
- Fix unreported: Fix: Border select for GPencil keyframes was including
  those in the "datablock" channels even though those weren't visible
  (blender/blender@f5d02f055f).
- Fix unreported: Fix: Deleting GPencil keyframes in DopeSheet didn't
  redraw the view
  (blender/blender@a1e8ef264f).
- Fix [\#52579](http://developer.blender.org/T52579): Alembic: crash
  when replacing slightly different alembic files
  (blender/blender@4b90830cac).
- Fix unreported: Cycles: FIx issue with -0 being considered a
  non-finite value
  (blender/blender@95d871c1a2).
- Fix [\#51907](http://developer.blender.org/T51907): New Depsgraph -
  Camera constraint is not evaluated properly
  (blender/blender@edded659c6).
- Fix [\#52533](http://developer.blender.org/T52533): Blender shuts down
  when rendering duplicated smoke domain
  (blender/blender@c9d653e560).
- Fix [\#52209](http://developer.blender.org/T52209): New Depsgraph -
  animated follow curve constraint sometimes freaks out when the curve
  has a parent
  (blender/blender@0bee126977).
- Fix unreported: Cycles Bake: Fix overflow when using hundreds of
  images
  (blender/blender@9ca3d4cbbd).
- Fix [\#52251](http://developer.blender.org/T52251): Knife cut
  displaces surface
  (blender/blender@a8c7f1329b).
- Fix [\#52374](http://developer.blender.org/T52374): Changes of rigid
  body related settings during simulation will break the simulation
  (blender/blender@d84f559555).
- Fix unreported: Rigidbody: Fix regression introduced in ee3fadd
  (blender/blender@e91f9f664d).
- Fix [\#52522](http://developer.blender.org/T52522): VSE renders with
  alpha transparent PNG image incorrectly
  (blender/blender@a8bd08ffdd).
- Fix [\#52663](http://developer.blender.org/T52663): Remap used invalid
  local-view data
  (blender/blender@b895c7337e).
- Fix [\#52678](http://developer.blender.org/T52678): Crash editing
  gpencil w/ frame-lock
  (blender/blender@3aaf908719).
- Fix unreported: UI: fix memory leak when copy-to-selected failed
  (blender/blender@9da098536d).
- Fix [\#52639](http://developer.blender.org/T52639): Weight paint
  smooth tool crash
  (blender/blender@300abf241e).
- Fix unreported: Fix transform snap code using 'allocated' flags to get
  verts/edges/etc. arrays again from DM
  (blender/blender@9cc7e32f39).
- Fix [\#52149](http://developer.blender.org/T52149): LoopTriArray
  computation was not correctly protected against concurrency
  (blender/blender@9ae35fafb6).
- Fix [\#52650](http://developer.blender.org/T52650): Grease pencil
  selection its not automatically updating in Clip Editor
  (blender/blender@87cc8550e2).
- Fix unreported: Cycles: Safer fix for infinite recursion
  (blender/blender@fbb4be061c).
- Fix [\#52531](http://developer.blender.org/T52531): Blender 2D
  stabilisation node issue when autoscale is selected
  (blender/blender@82466852fe).
- Fix unreported: PyAPI: Fix mathutils freeze allowing owned data
  (blender/blender@1a7dda046b).
- Fix [\#52701](http://developer.blender.org/T52701): Mesh shortest path
  fails at boundaries
  (blender/blender@904831e62e).
- Fix [\#52696](http://developer.blender.org/T52696): Sculpt - Brush
  spacing pressure artifacts
  (blender/blender@9a9e9b1c4d).
