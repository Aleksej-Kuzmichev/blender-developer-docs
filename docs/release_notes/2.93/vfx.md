# Blender 2.93: VFX & Video

## FFmpeg

- Improve scrubbing performance
  (blender/blender@88604b79b7d1)
- Improve proxy building performance
  (blender/blender@e4f347783320)
- Improve threading settings
  (blender/blender@1614795ae2bf)
- Adjust default proxy settings
  (blender/blender@b9207fb43d6f)

## Motion tracking

- Add track averaging operator
  (blender/blender@f17b26bbed3)
- Resolved old-standing issue with Lock-to-View causing jumps when
  changing selection
  (blender/blender@66f8835f9c03).
  The fix also avoids jump when toggling lock-to-selection option.

## Sequencer

- Inherit blend mode with single input effects
  (blender/blender@d6bbcc4f666b)
- Automatic proxy building
  (blender/blender@04e1feb83051)
- Simplify proxy settings
  (blender/blender@91561629cd0b)
- Preview images when moving strip handles
  (blender/blender@3d9ee83d8818)
- Text strip improvements
  (blender/blender@1c095203c275)
- Add bold and italic option for text strip
  (blender/blender@913b71bb8be9)
- Add new_meta RNA API function
  (blender/blender@bfad8deb0be0)
- Add move_to_meta RNA API function
  (blender/blender@4158405c0b9d)
- Disk cache path was changed to include `_seq_cache` suffix
  (blender/blender@5368859a669d)

## Compositor

**Redesigned Cryptomatte Workflow**
blender/blender@d49e7b82da67885aac5933e094ee217ff777ac03

In the redesigned workflow a RenderLayer or Image can be selected
directly from the cryptomatte node. The cryptomatte layers are extracted
from the meta data. No need anymore to connect the sockets from the
render layer node anymore.

![Sampling a matte using the Cryptomatte node](../../images/20210413_cryptomatte-workflow.png)

When picking a cryptomatte id the mouse shows a hint what is underneath
the mouse cursor. There is no need anymore to switch to the pick socket.
You can even pick from the image editor or movie clip editor.

The matte id's are stored as readable text in the node. It is even
possible to alter the text.

**Anti Aliasing Node**
blender/blender@805d9478109e

The Anti-Aliasing node removes distortion artifacts around edges known
as aliasing. The implementation is based on Enhanced Subpixel
Morphological Antialiasing.

![Anti aliasing node](../../images/20210413_anti-aliasing-node.png)

## Animation Player

- Improved display performance using GLSL to convert the color-space
  instead of the CPU for float images
  blender/blender@fd3e44492e7606a741a6d2c42b31687598c7d09a.
- Added support for limiting cache using a memory limit instead of a
  fixed number of frames.  
  This uses the memory cache limit system-preference when launching the
  player from Blender, otherwise it can be accessed with via a new
  command line argument `-c <memory_limit_mb>`.
  blender/blender@3ee49c8711ca72b03687574a98adec904ec1699c
- Image cache is now filled at startup to avoid frame-skipping during
  playback as frames load
  blender/blender@3ee49c8711ca72b03687574a98adec904ec1699c
