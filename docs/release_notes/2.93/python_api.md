# Blender 2.93: Python API

## Python 3.9

Python was upgraded to [version
3.9.1](https://www.python.org/downloads/release/python-391/).

## GPU

The `gpu` module has been enhanced to cover more functionality that
previously was only available through `bgl`.
(blender/blender@4430e8a008,
blender/blender@6c6b1c015b)

The `bgl` module will be deprecated in Blender 3.0, in preparation of
future Vulkan support. Add-ons should switch to using the `gpu`
module, so that they will work with both OpenGL and Vulkan.

- New `state` sub-module for changing drawing state.
- New `texture` sub-module, including a function to create textures
  from image datablocks.
- New `GPUFrameBuffer`
- New `GPUUniformBuf`

## Other Additions

- New `RenderEngine.bl_use_custom_freestyle` option. By default this
  is disabled, and Freestyle rendering will be done by Eevee. If
  enabled, the render engine will be called to render the Freestyle
  geometry.
  (blender/blender@14285445283ecc8890f7e1c4b4eb92cef3f0a8ed)
- New `to_curve` method in Object ID. The method is analogous to
  `to_mesh` and can be used to get the splines representing a text
  object or to get the splines after spline modifiers are applied.
  (blender/blender@f2c0bbed1ce5270eee1332a02da02c1819bb230c)
- New `CurveMapping.reset_view` method
  (blender/blender@3eb8307160e327f64593d218125d02289285aa17).
- New `CurveProfile.reset_view` method
  (blender/blender@7e3efac9a85743dbe6a3646170a4f4475bf91e6c).
- New `BlendFile.temp_data` method, providing a context manager to
  temporarily load blend file data without linking/appending it into the
  current file
  (blender/blender@9e0921497912cbfe9846358d1cb1220f88315f80).
- User Interface: Add support for `bl_description` and python doc-strings for
  panel classes.
  (blender/blender@8971018eb64a)

## Other Changes

- UTF8 is now the default encoding on all platforms, matching the
  behavior of running `python -X utf8`. See
  [PEP-540](https://www.python.org/dev/peps/pep-0540)
  (blender/blender@df135b74fc938ec5ff08bc912ece18917165319d).
- The `deform` parameter of the Bmesh `from_object` method is now
  deprecated, always assumed to be True, and will be removed in version
  3.0.
  (blender/blender@4b0871af8716c52741190c2f9c57894378f91e72)
- The intermediate representation of `bpy.props`, typically defined in
  a classes annotations before registration is now using a new type
  `bpy.props._PropertyDeferred`. While this is not considered part of
  the stable API, some scripts depended on this
  (blender/blender@c44c611c6d8c6ae071b48efb5fc07168f18cd17e)
- `bpy.ops.mesh.primitive_grid_add` the resulting subdivision levels
  has been changed by n+1
  (blender/blender@4d3cdb32d399335dba72c180309c0d34f93cd533).
- Remove support for non-annotation properties in classes as this was
  only enabled while porting scripts to 2.8x API
  (blender/blender@afa5da9ce02bcd46f70d250943a933afe6de1d59)

  
Scripts that dynamically generate types will need to be updated, see the
following example for reference:

Before (2.92 or older)  

```python
import bpy
settings_class = type(
    "TestClass",
    (bpy.types.PropertyGroup,), {
        "test": bpy.props.StringProperty(default="test"),
    },
)
bpy.utils.register_class(settings_class)
bpy.types.WindowManager.example = bpy.props.PointerProperty(type=settings_class)
print(bpy.context.window_manager.example.test)
```

After (2.93 and newer)
```python
import bpy
settings_class = type(
    "TestClass",
    (bpy.types.PropertyGroup,), {
        "__annotations__": {
            "test": bpy.props.StringProperty(default="test"),
        },
    },
)
bpy.utils.register_class(settings_class)
bpy.types.WindowManager.example = bpy.props.PointerProperty(type=settings_class)
print(bpy.context.window_manager.example.test)
```
