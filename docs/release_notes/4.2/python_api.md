# Blender 4.2: Python API & Text Editor

## Text Editor

- Support for GLSL syntax highlighting
  (blender/blender@462c144f414343ffbbac3546f1fae2bbf0bd52db)

## Additions

- Add `bpy.utils.register_cli_command` & `unregister_cli_command`
  so scripts can define their own command line functionality via (`-c`, `--command`).
  (blender/blender@9372e0dfe092e45cc17b36140d0a3182d9747833)

## Breaking changes

### Render Settings

- Motion Blur settings have been de-duplicated between Cycles and EEVEE and moved to
  `bpy.types.RenderSettings`. (blender/blender@74b8f99b43)
  - `scene.cycles.motion_blur_position` -> `scene.render.motion_blur_position`
  - `scene.eevee.use_motion_blur` -> `scene.render.user_motion_blur`
  - `scene.eevee.motion_blur_position` -> `scene.render.motion_blur_position`
  - `scene.eevee.motion_blur_shutter` -> `scene.render.motion_blur_shutter`

### Nodes

- Unused `parent` argument removed from the `NodeTreeInterface.new_panel` function
  for creating node group panels. (blender/blender#118792)

### Image Object Operators

- The "add image object" operator has been deduplicated and unified into one operator: `object.empty_image_add`. (blender/blender@013cd3d1ba)  
  There were two operators written in python (`object.load_reference_image` and `object.load_background_image`) and an other written in C/C++ (`object.drop_named_image`) prior to this change.
- The "add/remove background image from camera" operator has been renamed to clarify that this is only for the camera object (blender/blender@013cd3d1ba):
  - `view3d.background_image_add` -> `view3d.camera_background_image_add`
  - `view3d.background_image_remove` -> `view3d.camera_background_image_remove`