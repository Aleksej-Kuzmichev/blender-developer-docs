# Core

### Custom Properties

Custom properties can now have a boolean type
(blender/blender@ef68a37e5d55).

### Datablocks

'Fake User' on linked data will now always enforce that linked
data-block to be considered as 'directly linked', i.e. never
automatically cleared by Blender on file save. See
blender/blender#105786
and
blender/blender@133dde41bb
for details.
