# Blender 3.0: Grease Pencil

## Line Art

- Speed up tile access.
  (blender/blender@6ad4b8b7)
- Better tolerance to faces perpendicular to view.
  (blender/blender@5b176b66)
- Threaded object loading.
  (blender/blender@1b07b7a0)
- Cached calculation for modifiers that are in the same stack.
  (blender/blender@247abdbf)
- Loose edge type and related chaining improvements.
  (blender/blender@841df831)
- Option to filter feature line using freestyle face mark.
  (blender/blender@3558bb8e)
- Occlussion effectiveness support for mesh material.
  (blender/blender@cf21ba37)
- Option to filter feature line with collection intersection masks.
  (blender/blender@d1e0059e)
- Camera overscan allows line art effective range to go beyond image
  frame, preventing strokes from ending right at the border.
  (blender/blender@8e9d06f5)
- Automatic crease with flat/smooth surfaces, which allows crease
  detection to follow smooth/flat shading and auto-smooth configuration.
  (blender/blender@c1cf66bff3c0)
- Stroke offset function, allow strokes to be interacting with scene
  depth without showing in-front of
  everything.(blender/blender@c3ef1c15)
- Custom camera allows line art to compute strokes from other cameras
  instead of the active
  one.(blender/blender@efbd3642)
- Trimming edges right at the image border, useful when having multiple
  camera setup where you want the border to be
  clean.(blender/blender@ec831ce5)

## Operators

- New operator to bake GPencil transformed strokes into a new GPencil
  object.
  (blender/blender@06f86dd4d9a2)
- Multiframe support added to Reproject operator.
  (blender/blender@e1acefd45e23)
- Multiframe support added to Move to Layer operator.
  (blender/blender@e4cebec647fb)
- Mask layer list now can be reordered.
  (blender/blender@8032bd98d820)
- Now Blank objects include Layer and Material by default.
  (blender/blender@5cb1e18f7207)
- Added support to convert Text to Grease Pencil object.
  (blender/blender@41820e8a8e70)
- New Bracket keymaps (`\[`, `\]`) to increase and decrease brush
  size.
  (blender/blender@c58bf31aeddd)
- New operator to Copy materials to other grease pencil object. Can copy
  Active or All materials.
  (blender/blender@960535ddf344),
  (blender/blender@f6cb9433d45a)
- Layer Copy to Object has been renamed to Copy Layer to Object and
  allows to copy all layers at once.
  (blender/blender@960535ddf344),
  (blender/blender@f6cb9433d45a)
- New operator to normalize the Thickness or the Opacity of Strokes.
  (blender/blender@f944121700da)
- New Select Random operator.
  (blender/blender@a7aeec26550e)
- New Convert Mesh to Grease Pencil copy the Vertex Groups with weights.
  (blender/blender@88dc274d0533)
- Automerge when drawing strokes has been improved for better join when
  thickness is very different.
  (blender/blender@ae334532cffb)

## Tools

- New `Dilate` parameter for Fill brush to expand filled area to fill
  small gaps.
  (blender/blender@f8f6e0b25621)
- Annotations: Restore the `Placement` parameter in 2D Editors.
  (blender/blender@6ee14c966d0)

## UI

- The Topbar `Leak Size` parameter has been replaced by new `Dilate`
  parameter. `Leak Size` has been moved to Advanced panel.
  (blender/blender@f8f6e0b25621)
- Removed duplicated `B` keymap to insert Blank keyframe in Drawing
  Mode. This was old keymap and it has been replaced with `I` menu.
  (blender/blender@04679794259f)
- Annotations now display Stabilizer parameters in topbar and also in
  VSE.
  (blender/blender@0a83f32d79bc)
- New `Use Lights` option now available for when creating line art
  GPencil objects to allow convenient access to desired effect.
  (blender/blender@45b28c0f8847)

![](../../images/GP_Use_Lights.png)

- New option in Topbar to define the type of caps (Rounded/Flat) used in
  new strokes.
  (blender/blender@c1730ed16568)
- New keymap `Shift+Ctrl+M`to merge layer.
  (blender/blender@27b9cb7a1e67)
- Added buttons to move Up and Down Vertex Groups in the list.
  (blender/blender@2b64b4d90d67)
- Use Scale Thickness option is now enabled by default in 2D template.
  (blender/blender@6fc92b296fae)
- List of modifiers reorganized moving some of them to new `Modify`
  column.
  (blender/blender@9cf593f30519)

## Modifiers and VFX

- New Vertex Weight modifiers to generate weights base on Proximity or
  Angle on the fly to be used in any modifier.
  (blender/blender@29b65f534512,
  blender/blender@368b56c9a132)
- New Randomize options for Offset modifier.
  (blender/blender@6a2bc40e0131)
- Now Offset modifier uses the weights in the randomize parameters.
  (blender/blender@b73dc36859e0)
- New Length modifier that allows modification to stroke length in
  percentage to its own length or in geometry space
  (blender/blender@cd1612376138),
  also supports extending strokes based on curvature
  (blender/blender@25e548c96b3d8).

<video src="../../../videos/Length_Modifier_Test.mp4" width="800" controls=""></video>

![](../../images/GPencil_Length.png){style="width:800px;"}

- New Dot-dash modifier that allows generating dot-dash lines and assign
  different materials to each segments.
  (blender/blender@a2c5c2b4068d)

![](../../images/GPencil_Dash.png){style="width:800px;"}

## Compositing

- Added a new option to use masks during view layer render
  (blender/blender@e459a25e6cbe).
  This toggle can be found in the

"Layers" -\> "Relations" panel called "Use Masks in Render". The option
is disabled if there is no view layer selected.

|||
|-|-|
|![](../../images/GPencil_disable_masks_viewlayer_(not_disabled).png){style="width:400px;"}|![](../../images/GPencil_disable_masks_viewlayer_(disabled).png){style="width:400px;"}|
