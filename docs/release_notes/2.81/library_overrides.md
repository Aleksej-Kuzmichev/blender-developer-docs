# Blender 2.81: Library Overrides

Library Overrides is the new system designed to replace and supersede
Proxies. Most types of linked data-blocks can be overridden, and the
properties of those overrides can then be edited. When the library data
change, unmodified properties of the override one will be updated
accordingly.

**It is considered as experimental**, and current proxy system will be
kept alongside for a few releases. Automatic conversion from proxies to
library overrides *is not planned*, there will be a manual operator to
do that, but results on complex characters are not guaranteed.

Compared to proxies, library overrides support:

- Multiple independent overrides of a same linked data (e.g. a whole
  character).
- Adding new modifiers and constraints, anywhere in the stack.
- Recursively chaining overrides (i.e. link and override overrides from
  another library file, etc.).

<table markdown>
<tbody markdown>
<tr markdown>
<td markdown><div class="note_title">
<p>''''''</p>
</div>
<div class="note_content">
<p>There are still many known TODO's/issues that have to be addressed.
Please check the Phabricator main task of the project, <a
href="http://developer.blender.org/T53500">#53500</a>, for more
details.</p>
</div></td>
</tr>
</tbody>
</table>

Please see the [reference
manual](https://docs.blender.org/manual/en/dev/files/linked_libraries/library_overrides.html)
for more info about library overrides usage.

## Technical Notes on Implementation

Internally, overrides are *local* data-blocks that are copied from and
keep a reference to their linked source. They also keep a list of
locally modified properties (using RNA paths).

When saving the .blend file, both data-blocks (local override and linked
source) are diff'ed, and all allowed differences are stored in the
.blend file.

When reading the .blend file, linked source are duplicated again into
local overrides, and then all the diffing data is re-applied on the
overrides.

This implies that even if you modify a "forbidden" property, on next
file reload it will be restored to the linked data-block's value.

## Known Limitations and Issues

Many more things could/will be made overridable. Focus has been on
making overriding of characters for posing/animation working first, as
an initial real-life test case, and direct proxy replacement.

Note that driven shape keys won't be working 'out of the box' currently
(see [\#70319](http://developer.blender.org/T70319) for details).

Since overrides are basically local data-blocks for most of Blender,
especially for tools and operators, there are still a lot of cases that
can lead to editing non-overridable properties. While those cases are
being hunted down, remember that any forbidden editing will be reset to
the linked data status on next file reload anyway.

Performances, especially of the "diffing" process (where Blender
compares override and linked source data, and generate the needed data
to store the overrides), can be slow. By default this is ran on most of
the undo steps, which can make working on heavy production scenes
painful. The easiest way to work around that issue currently is to
disable the 'auto override' feature, at least for the Armature objects
(from python console, see \[the API doc
<https://docs.blender.org/api/master/bpy.types.IDOverrideLibrary.html#bpy.types.IDOverrideLibrary.auto_generate>\]).
This will prevent immediate automatic detection of edited (and hence
overridden) properties for those data-blocks, which will only happen on
file save.

As with proxies, in case a link to a library is broken, using the
Relocate feature (in the Outliner) won't work really well, bone shapes
e.g. are likely to be lost... Some data are currently lost in the
"place-holder" step of missing data-blocks.

Overriding a geometry object (especially a mesh) copies locally all of
the data, even though geometry itself is not (and will never be)
overridable.

## Future Ideas

There are several potential future extensions/improvements to library
overrides, which are currently not exposed to the user (though some are
partially implemented):

- Differential overrides (for scaling e.g., store the difference as a
  factor of the original value, instead of an absolute value).
- Protected/locked properties: forbid (by default) to edit some
  properties in the override data-blocks.
- Templates: define in the library files how a given data-block should
  be overridden by default (which properties should be overridden, which
  should be protected from modifications, etc.).
