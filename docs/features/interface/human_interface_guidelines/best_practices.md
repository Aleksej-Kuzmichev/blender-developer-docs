# Best Practices

- **Keep the UI calm.** Avoid strong changes in colors, big jumps in
  layouts, or popping elements on simple interactions. For example
  strong mouse hover highlights can be very flashy and cause distracting
  visual noise; some subtle changes can be okay.
- **Avoid accent colors when there's no need to grab the user's
  attention.** Using accent colors when there's no need to can add
  visual noise that distracts from more important parts of the screen.
  The GUI should be calm with subtle contrasts unless there's a good
  reason not to. Currently Blender uses accent colors too freely.
- **Inconsistencies should be well founded and documented.** There are
  cases where consistency hurts usability more than it helps. Breaking
  can be an acceptable trade-off *if* there are good reasons to do so -
  but only then.

## Workflow Oriented

**Emphasize actions, not things** (verbs over nouns). It's a common
fallacy to think everything needs a name, and that this name needs to be
a noun. Instead, put yourself in the users mind: What is the simplest,
most natural way to describe what the user is trying to do? And how can
the UI be modeled as close as possible to that? Turns out that sometimes
you don't even need to name a thing, just expose the possible options
directly. Entities and names often just add unnecessary abstractions.
Verbs are a powerful language construct, and are often a lot closer to
the user's mental model.

Examples:

- What is a more natural/usable expression: *"Make a vertex selection"*
  or *"select a vertex"*? *"Make a connection between two vertices"*,
  *"Create an edge between two vertices"* or *"connect two vertices"*?
- The context menu is an action/workflow oriented UI: A simple right
  click immediately provides common actions (usually a list of verbs).
  You don't have to first open a *Context* (noun) \> *Common Actions*
  (another noun) menu first.
- Instead of a *Text* or *Font* menu, place text formatting (bold,
  italic, etc.) in a *Format* menu.
- A menu entry like *Create Asset Data* could be named *Package as
  Asset* or *Mark as Asset* instead.

Current Blender is not following this guideline well.
