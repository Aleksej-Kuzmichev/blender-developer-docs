# Tools

There are tools that help when developing inside the GPU or drawing code.
Here is a list of commonly used tools.

- Validate shaders when compiling Blender using [Shader builder](./shader_builder.md)
- GPU debugging [Renderdoc](./renderdoc.md)
