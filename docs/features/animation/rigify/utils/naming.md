# Naming Utilities (rigify.utils.naming)

This module provides utilities for manipulating bone names.

## Name Decomposition

Rigify expects bone names to follow a certain structure, consisting of a
prefix, base name, Z and X sides, and a number, e.g. `DEF-foo.T.L.001`
(all but the base are optional). These utilities help with parsing and
recombining the names.

### NameParts Tuple

Decomposed names are represented as a named tuple with the following
fields:

`prefix`  
:   Optional prefix part (without the `-` delimiter) or None.

`base`  
:   The base part of the name.

`side_z`  
:   Optional vertical side tag, consisting of a `._-` delimiter and
    `TtBb` top or bottom side descriptor.

`side`  
:   Optional horizontal side tag, consisting of a `._-` delimiter and
    `LlRr` left or right side descriptor.

`number`  
:   Optional number tag without the `.` delimiter.

### Functions

`split_name(name)`  
:   Parses the name string into the `NameParts` tuple representation.

`combine_name(parts, *, prefix=None, base=None, side_z=None, side=None, number=None)`  
:   Recombines the tuple into a name string, optionally replacing some of
    its parts. The `number` part may be specified as an int.

## Name Mirroring

These utilities deal with name symmetry along X and Z axes.

### Side Enum

The horizontal side can be represented as the `Side` IntEnum.

`Side.LEFT = -1`  
:   Left side.

`Side.MIDDLE = 0`  
:   Neutral.

`Side.RIGHT = +1`  
:   Right side.

Functions:

`Side.from_parts(parts)`  
:   Convert the side component of the name parts tuple to the enum value.

`Side.to_string(parts, side)`  
:   Convert the side enum value to a name side component, matching the
    delimiter and case from the tuple.

`Side.to_name(parts, side)`  
:   Replace the name side component using the side enum value.

### SideZ Enum

The vertical side can be represented as the `SideZ` IntEnum.

`SideZ.TOP = +2`  
:   Top side.

`SideZ.MIDDLE = 0`  
:   Neutral.

`SideZ.BOTTOM = -2`  
:   Bottom side.

Functions:

`SideZ.from_parts(parts)`  
:   Convert the `side_z` component of the name parts tuple to the enum value.

`SideZ.to_string(parts, side)`  
:   Convert the side enum value to a name `side_z` component, matching the
    delimiter and case from the tuple.

`SideZ.to_name(parts, side)`  
:   Replace the name `side_z` component using the side enum value.

### NameSides Tuple

Names can be decomposed into a neutral name and two side enum values,
represented as a named tuple.

`base`  
:   Name string with sides removed.

`side`  
:   Horizontal `Side` value.

`side_z`  
:   Vertical `SideZ` value.

### Functions

`get_name_side(name)`  
:   Extract the horizontal `Side` value from the name string.

`get_name_side_z(name)`  
:   Extract the vertical `SideZ` value from the name string.

`get_name_base_and_sides(name)`  
:   Split the name into a `NameSides` tuple of a neutral name and both
    side values.

`change_name_side(name, side=None, *, side_z=None)`  
:   Replace the sides components of the name using the enum values.

`mirror_name(name)`  
:   Mirror the name in the horizontal direction.

`mirror_name_z(name)`  
:   Mirror the name in the vertical direction.

## Utility Functions

General utilities, built on top of the above low-level functions:

`get_name(bone)`  
:   Return the name of the bone, or None if bone is None.

`make_derived_name(name, subtype, suffix=None)`  
:   Replaces the prefix with one appropriate to the subtype (one in
    `'org'`, `'def'`, `'mch'`, `'ctrl'`), and optionally adds a
    suffix to the base part.

`is_control_bone(name)`  
:   Returns true if the name has no prefix component.

`strip_trailing_number(name)`  
:   Removes the number component of the name.

`strip_prefix(name)`  
:   Removes the prefix component of the name.

`strip_org(name)`  
:   Removes only the ORG prefix from the name.

`strip_mch(name)`  
:   Removes only the MCH prefix from the name.

`strip_def(name)`  
:   Removes only the DEF prefix from the name.

`make_original_name(name)`, `org(name)`  
:   Adds the ORG prefix to the name if not there.

`make_mechanism_name(name)`, `mch(name)`  
:   Adds the MCH prefix to the name if not there.

`make_deformer_name(name)`, `deformer(name)`  
:   Adds the DEF prefix to the name if not there.

`unique_name(collection, name)`  
:   Find first prefix number that doesn't exist in the collection.

`random_id(length=8)`  
:   Generate a random alphanumeric string.

`choose_derived_bone(generator, original_name, subtype, *, by_owner=True, recursive=True)`  
:   Find a bone of the appropriate `make_derived_name` subtype that was
    generated based on the original bone.
    
    `by_owner` limits search to the original's owner rig, and
    `recursive` walks through chained copies.
